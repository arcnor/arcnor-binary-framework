/**
 * Copyright 2016 Edu Garcia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arcnor.binary

import java.nio.ByteOrder

enum class ABFNumberType(val bits: Int) {
	UINT8(8),
	UINT16(16),
	UINT24(24),
	UINT32(32),
	UINT64(64),

	SINT8(8),
	SINT16(16),
	SINT24(24),
	SINT32(32),
	SINT64(64),

	FLOAT(32),
	DOUBLE(64)
}

enum class ABFEnumType {
	/**
	 * Import/export the enum ordinal
	 */
	ORDINAL,
	/**
	 * Import/export the enum name. Use the `@ABFString` / `@ABFFixedString` annotations to control
	 * the processing of the string.
	 */
	NAME,
	/**
	 * Import/export whatever the `toString()` method of the enum returns. Use the `@ABFString` / `@ABFFixedString`
	 * annotations to control the processing of the string.
	 */
	TOSTRING
}

enum class ABFEndianness(val nioOrder: ByteOrder?) {
	LITTLE(ByteOrder.LITTLE_ENDIAN), BIG(ByteOrder.BIG_ENDIAN), INHERIT(null)
}
