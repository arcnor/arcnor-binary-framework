/**
 * Copyright 2016 Edu Garcia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arcnor.binary.export

import com.arcnor.binary.ABFEndianness
import com.arcnor.binary.ABFEnumType
import com.arcnor.binary.ABFNumberType
import com.arcnor.binary.ABFNumberType.DOUBLE
import com.arcnor.binary.ABFNumberType.FLOAT
import com.arcnor.binary.ABFNumberType.SINT16
import com.arcnor.binary.ABFNumberType.SINT24
import com.arcnor.binary.ABFNumberType.SINT32
import com.arcnor.binary.ABFNumberType.SINT64
import com.arcnor.binary.ABFNumberType.SINT8
import com.arcnor.binary.ABFNumberType.UINT16
import com.arcnor.binary.ABFNumberType.UINT24
import com.arcnor.binary.ABFNumberType.UINT32
import com.arcnor.binary.ABFNumberType.UINT64
import com.arcnor.binary.ABFNumberType.UINT8
import com.arcnor.binary.common.ABFArray
import com.arcnor.binary.common.ABFBitMask
import com.arcnor.binary.common.ABFEnum
import com.arcnor.binary.common.ABFFixedString
import com.arcnor.binary.common.ABFNumber
import com.arcnor.binary.common.ABFObject
import com.arcnor.binary.common.ABFProcessor
import com.arcnor.binary.common.ABFString
import com.arcnor.binary.common.ABFNumberValue
import com.arcnor.binary.util.DynamicByteBuffer
import com.arcnor.sp.data.SPPrimitiveValue
import com.arcnor.sp.data.SPValue
import com.arcnor.sp.data.SPValueArray
import com.arcnor.sp.data.SPValueArrayBoolean
import com.arcnor.sp.data.SPValueArrayByte
import com.arcnor.sp.data.SPValueArrayDouble
import com.arcnor.sp.data.SPValueArrayFloat
import com.arcnor.sp.data.SPValueArrayInt
import com.arcnor.sp.data.SPValueArrayLong
import com.arcnor.sp.data.SPValueArrayShort
import com.arcnor.sp.data.SPValueDouble
import com.arcnor.sp.data.SPValueLong
import com.arcnor.sp.data.SPValueMap
import com.arcnor.sp.data.SPValueString
import java.io.File
import java.io.OutputStream
import java.util.*
import kotlin.reflect.KCallable
import kotlin.reflect.KProperty1
import kotlin.reflect.declaredMemberProperties
import kotlin.reflect.jvm.isAccessible
import kotlin.reflect.primaryConstructor

class ABFExport(private val obj: Any) : ABFProcessor() {
	companion object {
		// FIXME: Simple caching!
		internal fun getExportableFields(objectClass: Class<*>): List<ExportableField> {
			val result = arrayListOf<ExportableField>()

			addExportableFields(result, objectClass, objectClass)

			return result
		}

		private fun addExportableFields(result: ArrayList<ExportableField>, objectClass: Class<*>, initialClass: Class<*>) {
			val clazz = objectClass
			if (clazz == Any::class.java)
				return

			val annotation = getAnnotation(ABFObject::class.java, clazz) ?: return

			addExportableFields(result, clazz.superclass as Class<Any>, initialClass)

			// Fields for abstract properties will appear on subclasses, and the field itself won't be part of the `ExportableField` (but the method will)... Not a big deal I think

			for (field in annotation.fields) {
				val exportableField = TemporaryABFField<KProperty1<Any, Any>>()
				var modified = false
				clazz.kotlin.declaredMemberProperties.firstOrNull { it.name.equals(field) }?.apply { exportableField.prop = this as KProperty1<Any, Any>; modified = true }
				clazz.declaredFields.firstOrNull { it.name.equals(field) }?.apply { exportableField.field = this; modified = true }
				clazz.declaredMethods.firstOrNull { val name = it.name; name == "get${field.capitalize()}" && !it.isVarArgs && it.parameterCount == 0 }?.apply { exportableField.getMethod = this; modified = true }

				if (!modified) {
					throw RuntimeException("Field '$field' not found on class '$clazz'")
				}
				val resultField = ExportableField(exportableField.field, exportableField.getMethod, exportableField.prop)
				result.add(resultField)
			}
		}
	}
	private data class ABFContextEntry(
			val numberValue: ABFNumberValue,
			val numberType: ABFNumberType,
			val value: SPValue,
			val valueMap: SPValueMap
	)

	private val parentBuffers = Stack<DynamicByteBuffer>()
	private val currentBuf = DynamicByteBuffer(0x100000)
	private val context = SPValueMap(null)
	private val toReEvaluate = mutableListOf<ABFContextEntry>()

	init {
		val rootAnnotation = obj.javaClass.getAnnotation(ABFObject::class.java) ?: throw RuntimeException("Root object '$obj' should have the @ABFObject annotation")
		if (rootAnnotation.endianness == ABFEndianness.INHERIT) throw RuntimeException("Root object '$obj' endianness must be defined (@ABFObject.endianness should be set to other than ABFEndianness.INHERIT)")

		currentBuf.order = rootAnnotation.endianness.nioOrder!!
	}

	fun export(out: String) {
		doProcessing()

		val f = File(out)
		currentBuf.dump(f)
	}

	fun export(outputStream: OutputStream) {
		doProcessing()

		currentBuf.dump(outputStream)
	}

	private fun doProcessing() {
		process(obj, context)
		if (parentBuffers.isNotEmpty()) {
			throw RuntimeException("Something went wrong, there are unprocessed parent buffers!")
		}

		reEvaluate()

		// Set root context's size
		var size = 0L
		context.keys.forEach { size += (context[it]!!.getProp(PROP_SIZE) as SPValueLong).value }
		context.setProp(PROP_SIZE, SPValueLong(size))
	}

	private fun reEvaluate() {
		for (entry in toReEvaluate) {
			val value = parse(entry.numberValue.exportSPExpr, entry.valueMap)
			val offset = entry.value.getProp(PROP_GLOBAL_OFFSET) as SPValueLong
			replaceNumber(entry.numberType, (value as SPPrimitiveValue<*>).value as Number, offset.value)
		}
	}

	private fun process(obj: Any?, currentContext: SPValueMap) {
		if (obj == null) return

		val clazz = obj.javaClass
		val abfObjectAnnotation = getAnnotation(ABFObject::class.java, clazz)
		val annotation = if (abfObjectAnnotation != null) abfObjectAnnotation else {
			println("Ignoring '$obj' of class '$clazz'")
			return
		}

		if (annotation.hasLength) {
			processNumberValue(ABFNumberValue("\$this#$PROP_SIZE"), annotation.lengthType, currentContext)
		}
		// FIXME: Needs to push (and pop at the end) current endianness
		if (annotation.endianness != ABFEndianness.INHERIT) {
			currentBuf.order = annotation.endianness.nioOrder!!
		}

		var offset = currentBuf.position
		val initialOffset = offset

		for (field in getExportableFields(clazz)) {
			val exportCondition = field.getAnnotation(ExportCondition::class.java)
			if (exportCondition != null) {
				if (!parseExportCondition(exportCondition, obj)) continue
			}
			val type = field.type

			val localOffset = currentBuf.position - initialOffset
			val newContext: SPValue? = when (type) {
			// Primitives
			// FIXME: Boolean!
				Byte::class.java, java.lang.Byte::class.java -> processNumber(obj, field, UINT8)
				Short::class.java, java.lang.Short::class.java -> processNumber(obj, field, UINT16)
				Int::class.java, java.lang.Integer::class.java -> processNumber(obj, field, UINT32)
				Long::class.java, java.lang.Long::class.java -> processNumber(obj, field, UINT64)
				Float::class.java, java.lang.Float::class.java -> processNumber(obj, field, FLOAT)
				Double::class.java, java.lang.Double::class.java -> processNumber(obj, field, DOUBLE)
				String::class.java, java.lang.String::class.java -> processString(obj, field)

			// Array of primitives
				// FIXME: Nullable arrays might not match this, fixed on the number side by importing `java.lang...`, but there is no class for Java arrays in Kotlin besides these
				BooleanArray::class.java -> processBooleanArray(obj, field)
				ByteArray::class.java -> processByteArray(obj, field)
				ShortArray::class.java -> processShortArray(obj, field)
				IntArray::class.java -> processIntArray(obj, field)
				LongArray::class.java -> processLongArray(obj, field)
				FloatArray::class.java -> processFloatArray(obj, field)
				DoubleArray::class.java -> processDoubleArray(obj, field)

			// Export specific
				ABFBitMask::class.java -> processBitmask(obj, field)
				ABFNumberValue::class.java -> processNumberValueExpr(obj, field, currentContext)

				else -> {
					if (type.isEnum) {
						processEnum(obj, field)
					} else if (type.isArray) {
						processArray(obj, field, currentContext)
					} else if (List::class.java.isAssignableFrom(type)) {
						processList(obj, field, currentContext)
					} else {
						processObject(field.getValue(obj), currentContext)
					}
				}
			}

			val newOffset = currentBuf.position

			if (newContext != null) {
				currentContext[field.name] = newContext

				val size = newOffset - offset

				newContext.setProp(PROP_SIZE, SPValueLong(size.toLong()))
				newContext.setProp(PROP_LOCAL_OFFSET, SPValueLong(localOffset.toLong()))
				newContext.setProp(PROP_GLOBAL_OFFSET, SPValueLong(offset))
			}

			offset = newOffset
		}
	}

	private fun processObject(value: Any?, currentContext: SPValueMap): SPValueMap? {
		if (value == null) return null
		val result = SPValueMap(currentContext)
		process(value, result)
		return result
	}

	private fun parseExportCondition(exportCondition: ExportCondition, obj: Any?): Boolean {
		val conditionClass = exportCondition.value
		val exportConditionObj = if (conditionClass.objectInstance != null) {
			conditionClass.objectInstance
		} else {
			val constructor: KCallable<ExportConditionMatcher> = conditionClass.primaryConstructor ?: throw RuntimeException("Condition classes need a primary constructor!")
			val previousAccessibility = constructor.isAccessible
			constructor.isAccessible = true

			val parameterCount = constructor.parameters.size

			val result = when (parameterCount) {
				0 -> constructor.call()
				1 -> constructor.call(obj)
				else -> throw RuntimeException("Only condition classes that accept 0 or 1 (for inner classes) parameters in their constructor are allowed")
			}

			constructor.isAccessible = previousAccessibility

			result
		}
		if (exportConditionObj?.match() == true) return true
		return false
	}

	private fun processNumberValueExpr(obj: Any, field: ExportableField, context: SPValueMap): SPValueLong? {
		val value = field.getValue(obj) as ABFNumberValue? ?: return null

		// FIXME: Invalid number type if it's DOUBLE/FLOAT or signed
		val numberAnnotation = field.getAnnotation(ABFNumber::class.java) ?: throw RuntimeException("ExportableNumberValue needs an @ABFNumber annotation to specify the size to be exported")

		return processNumberValue(value, numberAnnotation.type, context)
	}

	private fun processNumberValue(value: ABFNumberValue, type: ABFNumberType, context: SPValueMap): SPValueLong {
		// FIXME: Remove this limitation
		if (type == DOUBLE || type == FLOAT) {
			throw UnsupportedOperationException("Number values cannot be of type Float/Double (for now)")
		}
		writeNumber(type, 0)

		val result = SPValueLong(0L)
		toReEvaluate.add(ABFContextEntry(value, type, result, context))
		return result
	}

	// FIXME: SPValueLong might not be the best result, we should have something like SPValueBitmask
	private fun processBitmask(obj: Any, field: ExportableField): SPValueLong? {
		val value = field.getValue(obj) as ABFBitMask? ?: return null
		// FIXME: Invalid number type if it's DOUBLE/FLOAT or signed
		val numberAnnotation = field.getAnnotation(ABFNumber::class.java) ?: throw RuntimeException("Bit masks need an @ABFNumber annotation to specify the size to be exported")
		var bitmask = 0L
		var idx = 0
		val bits = BooleanArray(value.size)
		for ((j, bit) in value.withIndex()) {
			if (bit) {
				bitmask = bitmask or (1L shl idx)
				bits[j] = true
			}
			idx++
		}
		val lengthType = numberAnnotation.type
		if (value.size > lengthType.bits) {
			throw RuntimeException(
					"Invalid lengthType for bitmask (number of bits to export is ${value.size}, " +
					"but lengthType '$lengthType' is ${lengthType.bits} bits long)"
			)
		}
		writeNumber(lengthType, bitmask)

		val result = SPValueLong(bitmask.toLong())
		result.setProp(PROP_BITS, SPValueArrayBoolean(bits))
		return result
	}

	private fun processBooleanArray(obj: Any, field: ExportableField): SPValueArrayBoolean? {
		throw NotImplementedError()
	}

	private fun processByteArray(obj: Any, field: ExportableField): SPValueArrayByte? {
		val value = field.getValue(obj) as ByteArray? ?: return null
		parseArrayLength(field, value.size)
		for (number in value) {
			writeNumber(UINT8, number)
		}

		return SPValueArrayByte(value)
	}

	private fun processShortArray(obj: Any, field: ExportableField): SPValueArrayShort? {
		val value = field.getValue(obj) as ShortArray? ?: return null
		parseArrayLength(field, value.size)
		for (number in value) {
			writeNumber(UINT16, number)
		}

		return SPValueArrayShort(value)
	}

	private fun processIntArray(obj: Any, field: ExportableField): SPValueArrayInt? {
		val value = field.getValue(obj) as IntArray? ?: return null
		parseArrayLength(field, value.size)
		for (number in value) {
			writeNumber(UINT32, number)
		}

		return SPValueArrayInt(value)
	}

	private fun processLongArray(obj: Any, field: ExportableField): SPValueArrayLong? {
		val value = field.getValue(obj) as LongArray? ?: return null
		parseArrayLength(field, value.size)
		for (number in value) {
			writeNumber(UINT64, number)
		}

		return SPValueArrayLong(value)
	}

	private fun processFloatArray(obj: Any, field: ExportableField): SPValueArrayFloat? {
		val value = field.getValue(obj) as FloatArray? ?: return null
		parseArrayLength(field, value.size)
		for (number in value) {
			writeNumber(FLOAT, number)
		}

		return SPValueArrayFloat(value)
	}

	private fun processDoubleArray(obj: Any, field: ExportableField): SPValueArrayDouble? {
		val value = field.getValue(obj) as DoubleArray? ?: return null
		parseArrayLength(field, value.size)
		for (number in value) {
			writeNumber(DOUBLE, number)
		}

		return SPValueArrayDouble(value)
	}

	private fun processArray(obj: Any, field: ExportableField, currentContext: SPValueMap): SPValueArray? {
		val value = field.getValue(obj) as Array<*>? ?: return null  // Null values skip the field altogether!

		return processArrayOrList(currentContext, field, value.size) { index: Int -> value[index] }
	}

	private fun processList(obj: Any, field: ExportableField, currentContext: SPValueMap): SPValueArray? {
		val value = field.getValue(obj) as List<*>? ?: return null  // Null values skip the field altogether!

		return processArrayOrList(currentContext, field, value.size) { index: Int -> value[index] }
	}

	private fun processArrayOrList(currentContext: SPValueMap, field: ExportableField, arraySize: Int, getValueAt: (index: Int) -> Any?): SPValueArray {
		var offset = currentBuf.position
		val initialOffset = offset

		parseArrayLength(field, arraySize)
		val array = Array<SPValue>(arraySize) { j ->
			val newContext = SPValueMap(currentContext)
			val localOffset = currentBuf.position - initialOffset

			process(getValueAt(j), newContext)

			val newOffset = currentBuf.position
			val size = newOffset - offset

			newContext.setProp(PROP_INDEX, SPValueLong(j.toLong()))
			newContext.setProp(PROP_SIZE, SPValueLong(size.toLong()))
			newContext.setProp(PROP_LOCAL_OFFSET, SPValueLong(localOffset.toLong()))
			newContext.setProp(PROP_GLOBAL_OFFSET, SPValueLong(offset))

			offset = newOffset

			newContext
		}
		return SPValueArray(array)
	}

	private fun parseArrayLength(field: ExportableField, length: Int) {
		val annotation = field.getAnnotation(ABFArray::class.java)
		val exportLength = annotation?.hasLength ?: true
		if (exportLength) {
			val lengthType = annotation?.lengthType ?: UINT32
			writeNumber(lengthType, length)
		}
	}

	// FIXME: SPValueString/Long might not be the best result
	private fun processEnum(obj: Any, field: ExportableField): SPValue? {
		val value = field.getValue(obj) as Enum<*>? ?: return null      // Null values skip the field altogether!
		val annotation = field.getAnnotation(ABFEnum::class.java)
		// FIXME: Get default values from annotation itself
		val exportType = annotation?.enumType ?: ABFEnumType.ORDINAL

		return when (exportType) {
			ABFEnumType.NAME -> processString(value.name, field)
			ABFEnumType.TOSTRING -> processString(value.toString(), field)
			else -> {
				val ordinalType = annotation?.ordinalType ?: UINT8
				writeNumber(ordinalType, value.ordinal)

				SPValueLong(value.ordinal.toLong())
			}
		}
	}

	private fun processString(obj: Any, field: ExportableField): SPValueString? {
		val value = field.getValue(obj) as String? ?: return null
		return processString(value, field)
	}

	private fun processString(value: String, field: ExportableField): SPValueString {
		val annotationFixed = field.getAnnotation(ABFFixedString::class.java)

		return if (annotationFixed != null) {
			// Fixed string
			val buf = ByteArray(annotationFixed.size)
			val length = Math.min(value.length, buf.size)
			System.arraycopy(value.toByteArray(), 0, buf, 0, length)
			currentBuf.writeByteArray(buf)

			SPValueString(value.substring(0, length))
		} else {
			// Variable size string
			val annotation = field.getAnnotation(ABFString::class.java)

			val alignment = annotation?.alignment ?: 0
			val zeroPadded = annotation?.zeroPadded ?: false
			val exportLength = annotation?.hasLength ?: false
			val lengthType = annotation?.lengthType ?: UINT8
			writeString(value, alignment, zeroPadded, exportLength, lengthType)

			SPValueString(value)
		}
	}

	private fun processNumber(obj: Any, field: ExportableField, defaultType: ABFNumberType): SPValue? {
		val value = field.getValue(obj) as Number? ?: return null
		val annotation = field.getAnnotation(ABFNumber::class.java)
		val type = annotation?.type ?: defaultType
		writeNumber(type, value)

		return when (type) {
			FLOAT, DOUBLE -> SPValueDouble(value.toDouble())
			else -> SPValueLong(value.toLong())
		}
	}

	// Writing helpers //

	private fun writeString(value: String, alignment: Int, zeroPadded: Boolean, exportLength: Boolean, lengthType: ABFNumberType) {
		if (zeroPadded) {
			throw NotImplementedError()
		} else {
			val size = value.toByteArray().size
			if (exportLength) {
				writeNumber(lengthType, size)
			}
			currentBuf.writeString(value)

			if (alignment > 0) {
				val mod = size % alignment
				if (mod > 0) {
					currentBuf.writeByteArray(ByteArray(alignment - mod))
				}
			}
		}
	}

	private fun writeNumber(type: ABFNumberType, value: Number) {
		when (type) {
			SINT8, UINT8 -> currentBuf.writeInt8(value.toInt())
			SINT16, UINT16 -> currentBuf.writeInt16(value.toInt())
			SINT24, UINT24 -> currentBuf.writeInt24(value.toInt())
			SINT32, UINT32 -> currentBuf.writeInt32(value.toInt())
			SINT64, UINT64 -> currentBuf.writeInt64(value.toLong())
			FLOAT -> currentBuf.writeFloat(value.toFloat())
			DOUBLE -> currentBuf.writeDouble(value.toDouble())
			else -> throw UnsupportedOperationException()
		}
	}

	private fun replaceNumber(type: ABFNumberType, value: Number, pos: Long) {
		when (type) {
			SINT8, UINT8 -> currentBuf.replaceInt8(pos, value.toInt())
			SINT16, UINT16 -> currentBuf.replaceInt16(pos, value.toInt())
//			SINT24, UINT24 -> currentBuf.replaceInt24(pos, value.toInt())
			SINT32, UINT32 -> currentBuf.replaceInt32(pos, value.toInt())
			SINT64, UINT64 -> currentBuf.replaceInt64(pos, value.toLong())
			FLOAT -> currentBuf.replaceFloat(pos, value.toFloat())
			DOUBLE -> currentBuf.replaceDouble(pos, value.toDouble())
			else -> throw UnsupportedOperationException()
		}
	}
}
