/**
 * Copyright 2016 Edu Garcia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arcnor.binary.util

import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.nio.BufferUnderflowException
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.channels.Channels
import java.nio.channels.FileChannel
import java.nio.channels.WritableByteChannel

class DynamicByteBuffer(estimatedSize: Int) {
	private val bufs = arrayListOf(ByteBuffer.allocate(estimatedSize))
	private var currentBuf = bufs[0]
	private var currentBufIdx = 0

	private var partialPosition = 0L

	init {
		currentBuf.order(ByteOrder.LITTLE_ENDIAN)
	}

	fun isValid() = currentBuf != null

	fun readUInt8() = dynaRead(1) { buf -> buf.get().toInt() and 0xFF }
	fun readUInt16() = dynaRead(2) { buf -> buf.short.toInt() and 0xFFFF }
	// TODO UInt24
	fun readUInt32() = dynaRead(4) { buf -> buf.int.toLong() and 0xFFFFFFFF }

	fun readSInt8() = dynaRead(1) { buf -> buf.get() }
	fun readSInt16() = dynaRead(2) { buf -> buf.short }
	// TODO SInt24
	fun readSInt32() = dynaRead(4) { buf -> buf.int }

	fun readSInt64() = dynaRead(8) { buf -> buf.long }

	fun readString(size: Int) = dynaRead(size) { buf ->
		String(readByteArray(size))
	}

	fun readByteArray(size: Int) = dynaRead(size) { buf ->
		val result = ByteArray(size);
		buf.get(result);
		result
	}

	// Writing
	fun writeInt8(value: Int) = dynaWrite(1) { buf -> buf.put(value.toByte()) }

	fun writeInt16(value: Int) = dynaWrite(2) { buf -> buf.putShort(value.toShort()) }
	fun writeInt32(value: Int) = dynaWrite(4) { buf -> buf.putInt(value) }
	fun writeInt64(value: Long) = dynaWrite(8) { buf -> buf.putLong(value) }
	fun writeFloat(value: Float) = dynaWrite(8) { buf -> buf.putFloat(value) }
	fun writeDouble(value: Double) = dynaWrite(8) { buf -> buf.putDouble(value) }

	fun replaceInt8(pos: Long, value: Int) = dynaReplace(pos, 1) { pos, buf -> buf.put(pos, value.toByte()) }
	fun replaceInt16(pos: Long, value: Int) = dynaReplace(pos, 2) { pos, buf -> buf.putShort(pos, value.toShort()) }
	fun replaceInt32(pos: Long, value: Int) = dynaReplace(pos, 4) { pos, buf -> buf.putInt(pos, value) }
	fun replaceInt64(pos: Long, value: Long) = dynaReplace(pos, 8) { pos, buf -> buf.putLong(pos, value) }
	fun replaceFloat(pos: Long, value: Float) = dynaReplace(pos, 4) { pos, buf -> buf.putFloat(pos, value) }
	fun replaceDouble(pos: Long, value: Double) = dynaReplace(pos, 8) { pos, buf -> buf.putDouble(pos, value) }

	fun writeInt24(value: Int) = dynaWrite(6) { buf ->
		if (buf.order() == ByteOrder.BIG_ENDIAN) {
			buf.put(((value and 0xFF0000) ushr 16).toByte())
			buf.put(((value and 0x00FF00) ushr 8).toByte())
			buf.put((value and 0x0000FF).toByte())
		} else {
			buf.put((value and 0x0000FF).toByte())
			buf.put(((value and 0x00FF00) ushr 8).toByte())
			buf.put(((value and 0xFF0000) ushr 16).toByte())
		}
	}

	// Using dynaWrite here is not the best solution, as we can split the string among different buffers, although
	// the same can be said about the previous numeric methods, but code will be more complex
	fun writeString(value: String) = dynaWrite(value.length) { buf -> buf.put(value.toByteArray()) }

	fun writeByteArray(value: ByteArray) = dynaWrite(value.size) { buf -> buf.put(value) }

	fun dump(f: File) {
		f.createNewFile()
		val fos = FileOutputStream(f)
		val ch = fos.channel

		dump(ch)
	}

	fun dump(output: OutputStream) {
		val ch = Channels.newChannel(output)

		dump(ch)
	}

	private fun dump(ch: WritableByteChannel) {
		flip()
		for (buf in bufs) {
			ch.write(buf)
		}
		ch.close()
	}

	/**
	 * Copies a dynamic buffer into this one. The original buffer will become invalid after this is done
	 */
	fun shallowCopy(other: DynamicByteBuffer) {
		closeAndLimitCurrentBuf()

		bufs.addAll(other.bufs)
		other.bufs.clear()

		currentBuf = other.currentBuf
		other.currentBuf = null

		currentBufIdx = bufs.size - 1

		partialPosition = 0
		bufs.forEach { partialPosition += it.position() }
		partialPosition -= currentBuf.position()
	}

	private fun closeAndLimitCurrentBuf() {
		val newLimit = currentBuf.capacity() - currentBuf.remaining()
		if (newLimit == 0) {
			// FIXME: This should be `bufs.dropLast(1)` but for some inexplicable reason, it doesn't work?
			// Discard this buffer completely
			bufs.remove(currentBuf)
			currentBufIdx--
		} else {
			currentBuf.limit(newLimit)
		}
	}

	/**
	 * Copies a dynamic buffer into this one. The original buffer will continue to be valid after this is done.
	 *
	 * This is slower than using shallowCopy()
	 */
	fun deepCopy(other: DynamicByteBuffer) {
		other.flip()
		for (buf in other.bufs) {
			while (buf.hasRemaining()) {
				val remaining = currentBuf.remaining()
				val otherRemaining = buf.remaining()
				if (remaining >= otherRemaining) {
					currentBuf.put(buf.array(), buf.position(), otherRemaining)
					buf.position(buf.position() + otherRemaining)
				} else {
					if (remaining > 0) {
						currentBuf.put(buf.array(), buf.position(), remaining)
						buf.position(buf.position() + remaining)
					}
					if (!currentBuf.hasRemaining()) {
						createAndSetNewBuffer(0x20)
					}
				}
			}
		}
	}

	private fun <T> dynaRead(wordSize: Int, callback: (ByteBuffer) -> T): T {
		val buf = if (currentBuf.remaining() >= wordSize) {
			currentBuf
		} else {
			if (bufs.size > currentBufIdx + 1) {
				getNextBuffer(writing = false)
			} else {
				null
			}
		}

		if (buf == null) {
			throw BufferUnderflowException()
		} else {
			return callback(buf)
		}
	}

	private fun dynaWrite(wordSize: Int, callback: (ByteBuffer) -> Unit) {
		val buf = if (currentBuf.remaining() >= wordSize) {
			currentBuf
		} else {
			if (bufs.size > currentBufIdx + 1) {
				getNextBuffer(writing = true)
			} else {
				createAndSetNewBuffer(wordSize)
			}
		}

		return callback(buf)
	}

	private fun dynaReplace(pos: Long, size: Int, callback: (Int, ByteBuffer) -> Unit) {
		var tempPos = pos

		for (buf in bufs) {
			if (tempPos < buf.limit()) {
				callback(tempPos.toInt(), buf)
				break
			} else {
				tempPos -= buf.limit()
			}
		}
	}

	private fun getNextBuffer(writing: Boolean): ByteBuffer {
		if (writing) {
			// Limit buffer to contents
			currentBuf.limit(currentBuf.position())
			partialPosition += currentBuf.position()
		}

		val result = bufs[++currentBufIdx]
		result.order(currentBuf.order())
		currentBuf = result
		return result
	}

	private fun createAndSetNewBuffer(minSize: Int): ByteBuffer {
		// Limit buffer to contents
		currentBuf.limit(currentBuf.position())
		partialPosition += currentBuf.position()

		val order = currentBuf.order()
		currentBuf = ByteBuffer.allocate(Math.max(0x100000, minSize))
		currentBuf.order(order)

		bufs.add(currentBuf)
		currentBufIdx++

		return currentBuf
	}

	fun flip() {
		for (buf in bufs) {
			buf.flip()
		}
		currentBuf = bufs[0]
		currentBufIdx = 0
		partialPosition = 0
	}

	var order: ByteOrder
		get() = currentBuf.order()
		set(value: ByteOrder) { currentBuf.order(value) }

	val position: Long
		get() = partialPosition + currentBuf.position()

	fun dumpToChannel(channel: FileChannel) {
		for (buf in bufs) {
			channel.write(buf)
		}
	}
}
