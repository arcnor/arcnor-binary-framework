/**
 * Copyright 2016 Edu Garcia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arcnor.binary.import

import com.arcnor.binary.ABFEnumType
import com.arcnor.binary.ABFNumberType
import com.arcnor.binary.common.ABFBitMask
import com.arcnor.binary.common.ABFEnum
import com.arcnor.binary.common.ABFFixedString
import com.arcnor.binary.common.ABFNumber
import com.arcnor.binary.common.ABFNumberValue
import com.arcnor.binary.common.ABFProcessor
import com.arcnor.binary.exception.ABFMissingAnnotationException
import com.arcnor.sp.data.SPPrimitiveValue
import com.arcnor.sp.data.SPValue
import com.arcnor.sp.data.SPValueArray
import com.arcnor.sp.data.SPValueArrayBoolean
import com.arcnor.sp.data.SPValueArrayByte
import com.arcnor.sp.data.SPValueArrayDouble
import com.arcnor.sp.data.SPValueArrayFloat
import com.arcnor.sp.data.SPValueArrayInt
import com.arcnor.sp.data.SPValueArrayLong
import com.arcnor.sp.data.SPValueArrayShort
import com.arcnor.sp.data.SPValueContainer
import com.arcnor.sp.data.SPValueDouble
import com.arcnor.sp.data.SPValueLong
import com.arcnor.sp.data.SPValueMap
import com.arcnor.sp.data.SPValueString

internal object ABFContextProcessing {
	fun preprocess(clazz: Class<*>, arraysToProcess: MutableMap<ABFProcessor.ImportableField, SPValueContainer<*, Int>>, currentContext: SPValueMap) {
		if (currentContext.valid) return

		val importableFields = ABFImport.getImportableFields(clazz)

		for (field in importableFields) {
			val type = field.type

			val newContext = when (type) {
			// Primitives
			// FIXME: Boolean!
				Byte::class.java, java.lang.Byte::class.java -> ABFContextProcessing.forNumber(field, ABFNumberType.UINT8)
				Short::class.java, java.lang.Short::class.java -> ABFContextProcessing.forNumber(field, ABFNumberType.UINT16)
				Int::class.java, java.lang.Integer::class.java -> ABFContextProcessing.forNumber(field, ABFNumberType.UINT32)
				Long::class.java, java.lang.Long::class.java -> ABFContextProcessing.forNumber(field, ABFNumberType.UINT64)
				Float::class.java, java.lang.Float::class.java -> ABFContextProcessing.forNumber(field, ABFNumberType.FLOAT)
				Double::class.java, java.lang.Double::class.java -> ABFContextProcessing.forNumber(field, ABFNumberType.DOUBLE)
				String::class.java, java.lang.String::class.java -> ABFContextProcessing.forString(field)

			// Array of primitives
			// FIXME: Nullable arrays might not match this, fixed on the number side by importing `java.lang...`, but there is no class for Java arrays in Kotlin besides these
				BooleanArray::class.java -> ABFContextProcessing.forBooleanArray(field)
				ByteArray::class.java -> ABFContextProcessing.forByteArray(field)
				ShortArray::class.java -> ABFContextProcessing.forShortArray(field)
				IntArray::class.java -> ABFContextProcessing.forIntArray(field)
				LongArray::class.java -> ABFContextProcessing.forLongArray(field)
				FloatArray::class.java -> ABFContextProcessing.forFloatArray(field)
				DoubleArray::class.java -> ABFContextProcessing.forDoubleArray(field)

				ABFBitMask::class.java -> ABFContextProcessing.forBitmask(field)
				ABFNumberValue::class.java -> ABFContextProcessing.forNumberValue(field)

				else -> {
					if (type.isEnum) {
						ABFContextProcessing.forEnum(field)
					} else if (type.isArray) {
						val newContext = ABFContextProcessing.forArray(field, currentContext)
						arraysToProcess[field] = newContext
						newContext
					} else if (List::class.java.isAssignableFrom(type)) {
						val newContext = ABFContextProcessing.forArray(field, currentContext)
						arraysToProcess[field] = newContext
						newContext
					} else {
						val newContext = ABFContextProcessing.forObject(field.type, currentContext)

						preprocess(field.type, arraysToProcess, newContext)

						newContext
					}
				}
			}

			currentContext[field.name] = newContext
		}
	}
	fun forNumber(field: ABFProcessor.ImportableField, defaultNumberType: ABFNumberType): SPValue {
		val annotation = field.getAnnotation(ABFNumber::class.java)

		val type = annotation?.type ?: defaultNumberType
		val value = when (type) {
			ABFNumberType.FLOAT, ABFNumberType.DOUBLE -> SPValueDouble(0.0, false)
			else -> SPValueLong(0, false)
		}

		value.setProp(ABFProcessor.PROP_SIZE, SPValueLong((type.bits / 8).toLong()))
		value.setProp(ABFProcessor.PROP_LOCAL_OFFSET, SPValueLong(0, false))
		value.setProp(ABFProcessor.PROP_GLOBAL_OFFSET, SPValueLong(0, false))

		return value
	}

	fun forString(field: ABFProcessor.ImportableField): SPValue {
		val annotation = field.getAnnotation(ABFFixedString::class.java)

		val result = SPValueString("", false)

		val (size, valid) = if (annotation != null) {
			annotation.size.toLong() to true
		} else {
			0L to false
		}
		result.setProp(ABFProcessor.PROP_SIZE, SPValueLong(size, valid))
		result.setProp(ABFProcessor.PROP_LOCAL_OFFSET, SPValueLong(0, false))
		result.setProp(ABFProcessor.PROP_GLOBAL_OFFSET, SPValueLong(0, false))

		return result
	}

	fun forBitmask(field: ABFProcessor.ImportableField): SPValue {
		val numberAnnotation = field.getAnnotation(ABFNumber::class.java) ?: throw ABFMissingAnnotationException(ABFNumber::class, field.name, "${ABFBitMask::class.simpleName} needs it to specify the size to be imported")

		val result = SPValueLong(0, false)
		result.setProp(ABFProcessor.PROP_SIZE, SPValueLong((numberAnnotation.type.bits / 8).toLong()))
		result.setProp(ABFProcessor.PROP_LOCAL_OFFSET, SPValueLong(0, false))
		result.setProp(ABFProcessor.PROP_GLOBAL_OFFSET, SPValueLong(0, false))

		return result
	}

	fun forNumberValue(field: ABFProcessor.ImportableField): SPValue {
		val numberAnnotation = field.getAnnotation(ABFNumber::class.java) ?: throw ABFMissingAnnotationException(ABFNumber::class, field.name, "${ABFNumberValue::class.simpleName} needs it to specify the size to be imported")

		val result = when (numberAnnotation.type) {
			ABFNumberType.FLOAT, ABFNumberType.DOUBLE -> SPValueDouble(0.0, false)
			else -> SPValueLong(0, false)
		}
		result.setProp(ABFProcessor.PROP_SIZE, SPValueLong((numberAnnotation.type.bits / 8).toLong()))
		result.setProp(ABFProcessor.PROP_LOCAL_OFFSET, SPValueLong(0, false))
		result.setProp(ABFProcessor.PROP_GLOBAL_OFFSET, SPValueLong(0, false))

		return result
	}

	fun forEnum(field: ABFProcessor.ImportableField): SPPrimitiveValue<*> {
		val annotation = field.getAnnotation(ABFEnum::class.java)
		// FIXME: Get default values from annotation itself
		val exportType = annotation?.enumType ?: ABFEnumType.ORDINAL

		val (value, size) = when (exportType) {
			ABFEnumType.NAME -> {
				val stringAnnotation = field.getAnnotation(ABFFixedString::class.java)
				SPValueString("", false) to if (stringAnnotation != null) {
					SPValueLong(stringAnnotation.size.toLong())
				} else {
					SPValueLong(0, false)
				}
			}
			ABFEnumType.TOSTRING -> SPValueString("", false) to SPValueLong(0, false)
			else -> {
				val ordinalType = annotation?.ordinalType ?: ABFNumberType.UINT8

				SPValueLong(0, false) to SPValueLong((ordinalType.bits / 8).toLong())
			}
		}

		value.setProp(ABFProcessor.PROP_SIZE, size)
		value.setProp(ABFProcessor.PROP_LOCAL_OFFSET, SPValueLong(0, false))
		value.setProp(ABFProcessor.PROP_GLOBAL_OFFSET, SPValueLong(0, false))

		return value
	}

	fun forArray(field: ABFProcessor.ImportableField, currentContext: SPValueMap): SPValueArray {
		val result = SPValueArray(emptyArray(), false)

		result.setProp(ABFProcessor.PROP_SIZE, SPValueLong(0, false))
		result.setProp(ABFProcessor.PROP_LOCAL_OFFSET, SPValueLong(0, false))
		result.setProp(ABFProcessor.PROP_GLOBAL_OFFSET, SPValueLong(0, false))

		return result
	}

	fun forObject(type: Class<*>, currentContext: SPValueMap): SPValueMap {
		val result = SPValueMap(currentContext, false)

		result.setProp(ABFProcessor.PROP_SIZE, SPValueLong(0, false))
		result.setProp(ABFProcessor.PROP_LOCAL_OFFSET, SPValueLong(0, false))
		result.setProp(ABFProcessor.PROP_GLOBAL_OFFSET, SPValueLong(0, false))

		return result
	}

	fun forBooleanArray(field: ABFProcessor.ImportableField): SPValueArrayBoolean {
		val result = SPValueArrayBoolean(booleanArrayOf(), false)

		result.setProp(ABFProcessor.PROP_SIZE, SPValueLong(0, false))
		result.setProp(ABFProcessor.PROP_LOCAL_OFFSET, SPValueLong(0, false))
		result.setProp(ABFProcessor.PROP_GLOBAL_OFFSET, SPValueLong(0, false))

		return result
	}

	fun forByteArray(field: ABFProcessor.ImportableField): SPValueArrayByte {
		val result = SPValueArrayByte(byteArrayOf(), false)

		result.setProp(ABFProcessor.PROP_SIZE, SPValueLong(0, false))
		result.setProp(ABFProcessor.PROP_LOCAL_OFFSET, SPValueLong(0, false))
		result.setProp(ABFProcessor.PROP_GLOBAL_OFFSET, SPValueLong(0, false))

		return result
	}

	fun forShortArray(field: ABFProcessor.ImportableField): SPValueArrayShort {
		val result = SPValueArrayShort(shortArrayOf(), false)

		result.setProp(ABFProcessor.PROP_SIZE, SPValueLong(0, false))
		result.setProp(ABFProcessor.PROP_LOCAL_OFFSET, SPValueLong(0, false))
		result.setProp(ABFProcessor.PROP_GLOBAL_OFFSET, SPValueLong(0, false))

		return result
	}

	fun forIntArray(field: ABFProcessor.ImportableField): SPValueArrayInt {
		val result = SPValueArrayInt(intArrayOf(), false)

		result.setProp(ABFProcessor.PROP_SIZE, SPValueLong(0, false))
		result.setProp(ABFProcessor.PROP_LOCAL_OFFSET, SPValueLong(0, false))
		result.setProp(ABFProcessor.PROP_GLOBAL_OFFSET, SPValueLong(0, false))

		return result
	}

	fun forLongArray(field: ABFProcessor.ImportableField): SPValueArrayLong {
		val result = SPValueArrayLong(longArrayOf(), false)

		result.setProp(ABFProcessor.PROP_SIZE, SPValueLong(0, false))
		result.setProp(ABFProcessor.PROP_LOCAL_OFFSET, SPValueLong(0, false))
		result.setProp(ABFProcessor.PROP_GLOBAL_OFFSET, SPValueLong(0, false))

		return result
	}

	fun forFloatArray(field: ABFProcessor.ImportableField): SPValueArrayFloat {
		val result = SPValueArrayFloat(floatArrayOf(), false)

		result.setProp(ABFProcessor.PROP_SIZE, SPValueLong(0, false))
		result.setProp(ABFProcessor.PROP_LOCAL_OFFSET, SPValueLong(0, false))
		result.setProp(ABFProcessor.PROP_GLOBAL_OFFSET, SPValueLong(0, false))

		return result
	}

	fun forDoubleArray(field: ABFProcessor.ImportableField): SPValueArrayDouble {
		val result = SPValueArrayDouble(doubleArrayOf(), false)

		result.setProp(ABFProcessor.PROP_SIZE, SPValueLong(0, false))
		result.setProp(ABFProcessor.PROP_LOCAL_OFFSET, SPValueLong(0, false))
		result.setProp(ABFProcessor.PROP_GLOBAL_OFFSET, SPValueLong(0, false))

		return result
	}

	fun postProcess(context: SPValueMap): Boolean {
		if (context.keys.size == 0) {
			return false
		}

		val contextGlobalOffsetProp = context.getProp(ABFProcessor.PROP_GLOBAL_OFFSET) as SPValueLong?
		val globalOffset = contextGlobalOffsetProp?.value ?: -1

		if (globalOffset < 0) {
			return false
		}

		var changed = false
		while (doPostProcess(context, globalOffset)) {
			changed = true
		}
		return changed
	}

	private fun doPostProcess(context: SPValueMap, globalOffset: Long): Boolean {
		var nextLocalOffset = -1L
		var nextGlobalOffset = -1L
		var offsetGlobalToLocal = -1L
		var current: SPValue? = null
		var prev: SPValue? = null
		var changed = false

		val contextSize = context.getProp(ABFProcessor.PROP_SIZE) as SPValueLong?

		context.keys.plus("").forEachIndexed { idx, nextKey ->
			val firstOne = idx == 1
			val lastOne = nextKey == ""

			if (current == null) {
				current = context[nextKey]
			} else {
				val next = if (lastOne) null else context[nextKey]

				val sizeProp = current!!.getProp(ABFProcessor.PROP_SIZE) as SPValueLong
				val localProp = current!!.getProp(ABFProcessor.PROP_LOCAL_OFFSET) as SPValueLong
				val globalProp = current!!.getProp(ABFProcessor.PROP_GLOBAL_OFFSET) as SPValueLong

				if (!localProp.valid) {
					if (offsetGlobalToLocal >= 0 && globalProp.valid) {
						localProp.value = globalProp.value - offsetGlobalToLocal
						localProp.valid = true

						changed = true
					} else if (firstOne) {
						localProp.value = 0
						localProp.valid = true
						changed = true
					} else {
						if (nextLocalOffset >= 0) {
							localProp.value = nextLocalOffset
							localProp.valid = true
							changed = true
						}
					}
				}

				if (!globalProp.valid) {
					if (offsetGlobalToLocal >= 0 && localProp.valid) {
						globalProp.value = offsetGlobalToLocal + localProp.value
						globalProp.valid = true

						changed = true
					} else if (firstOne) {
						globalProp.value = globalOffset
						globalProp.valid = true
						changed = true
					} else {
						if (nextGlobalOffset >= 0) {
							globalProp.value = nextGlobalOffset
							globalProp.valid = true
							changed = true
						}
					}
				}

				if (localProp.valid && sizeProp.valid) {
					nextLocalOffset = localProp.value + sizeProp.value
				} else {
					nextLocalOffset = -1L
				}

				if (globalProp.valid && sizeProp.valid) {
					nextGlobalOffset = globalProp.value + sizeProp.value
				} else {
					nextGlobalOffset = -1L
				}

				if (firstOne && localProp.valid && globalProp.valid) {
					offsetGlobalToLocal = globalProp.value - localProp.value
				}

				if (!sizeProp.valid) {
					val nextLocalProp = next?.getProp(ABFProcessor.PROP_LOCAL_OFFSET) as SPValueLong?
					val nextGlobalProp = next?.getProp(ABFProcessor.PROP_GLOBAL_OFFSET) as SPValueLong?

					propArithmetic(sizeProp, nextLocalProp ?: contextSize, localProp) { values ->
						sizeProp.value = values[0].value - values[1].value
					}
					propArithmetic(sizeProp, nextGlobalProp ?: contextSize, globalProp) { values ->
						sizeProp.value = values[0].value - values[1].value
					}

					if (sizeProp.valid) {
						changed = true
					}
				}

				prev = current
				current = context[nextKey]
			}
		}

		return changed
	}

	private fun propArithmetic(prop: SPValueLong, vararg values: SPValueLong?, callback: (values: Array<SPValueLong>) -> Unit) {
		if (prop.valid) return

		values.forEach {
			if (it == null) return
			if (!it.valid) return
		}
		callback(values as Array<SPValueLong>)
		prop.valid = true
	}
}