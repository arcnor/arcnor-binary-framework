/**
 * Copyright 2016 Edu Garcia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arcnor.binary.import

import com.arcnor.binary.ABFEndianness
import com.arcnor.binary.ABFEnumType
import com.arcnor.binary.ABFNumberType
import com.arcnor.binary.common.ABFArray
import com.arcnor.binary.common.ABFBitMask
import com.arcnor.binary.common.ABFEnum
import com.arcnor.binary.common.ABFFixedString
import com.arcnor.binary.common.ABFImportArraySize
import com.arcnor.binary.common.ABFImportObjectMaps
import com.arcnor.binary.common.ABFNumber
import com.arcnor.binary.common.ABFNumberValue
import com.arcnor.binary.common.ABFObject
import com.arcnor.binary.common.ABFProcessor
import com.arcnor.binary.common.ABFString
import com.arcnor.binary.exception.ABFMissingAnnotationException
import com.arcnor.sp.data.SPPrimitiveValue
import com.arcnor.sp.data.SPValue
import com.arcnor.sp.data.SPValueArray
import com.arcnor.sp.data.SPValueArrayBoolean
import com.arcnor.sp.data.SPValueArrayByte
import com.arcnor.sp.data.SPValueArrayDouble
import com.arcnor.sp.data.SPValueArrayFloat
import com.arcnor.sp.data.SPValueArrayInt
import com.arcnor.sp.data.SPValueArrayLong
import com.arcnor.sp.data.SPValueArrayShort
import com.arcnor.sp.data.SPValueBoolean
import com.arcnor.sp.data.SPValueContainer
import com.arcnor.sp.data.SPValueLong
import com.arcnor.sp.data.SPValueMap
import com.arcnor.sp.data.SPValueString
import java.io.File
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.channels.FileChannel
import java.util.ArrayList
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.declaredMemberProperties

class ABFImport<T>(val rootClass: Class<T>) : ABFProcessor() {
	companion object {
		// FIXME: Signed versus unsigned
		internal fun readNumber(bb: ByteBuffer, type: ABFNumberType): Number {
			return when (type) {
				ABFNumberType.SINT8 -> bb.get()
				ABFNumberType.SINT16 -> bb.short
				ABFNumberType.SINT24 -> throw UnsupportedOperationException()
				ABFNumberType.SINT32 -> bb.int
				ABFNumberType.SINT64 -> bb.long

				ABFNumberType.UINT8 -> bb.get()
				ABFNumberType.UINT16 -> bb.short
				ABFNumberType.UINT24 -> throw UnsupportedOperationException()
				ABFNumberType.UINT32 -> bb.int
				ABFNumberType.UINT64 -> bb.long

				ABFNumberType.FLOAT -> bb.float
				ABFNumberType.DOUBLE -> bb.double
				else -> throw UnsupportedOperationException()
			}
		}

		internal fun skipNumber(bb: ByteBuffer, type: ABFNumberType): Number {
			val offset = when (type) {
				ABFNumberType.SINT8 -> 1
				ABFNumberType.SINT16 -> 2
				ABFNumberType.SINT24 -> 3
				ABFNumberType.SINT32 -> 4
				ABFNumberType.SINT64 -> 8

				ABFNumberType.UINT8 -> 1
				ABFNumberType.UINT16 -> 2
				ABFNumberType.UINT24 -> 3
				ABFNumberType.UINT32 -> 4
				ABFNumberType.UINT64 -> 8

				ABFNumberType.FLOAT -> 4
				ABFNumberType.DOUBLE -> 8
				else -> throw UnsupportedOperationException()
			}
			bb.position(bb.position() + offset)

			return offset
		}

		// FIXME: Simple caching!
		internal fun getImportableFields(objectClass: Class<*>): List<ImportableField> {
			val result = arrayListOf<ImportableField>()

			addImportableFields(result, objectClass, objectClass)

			return result
		}

		private fun addImportableFields(result: ArrayList<ImportableField>, objectClass: Class<*>, initialClass: Class<*>) {
			val clazz = objectClass
			if (clazz == Any::class.java)
				return

			val annotation = getAnnotation(ABFObject::class.java, clazz) ?: return

			addImportableFields(result, clazz.superclass as Class<Any>, initialClass)

			// Fields for abstract properties will appear on subclasses, and the field itself won't be part of the `ExportableField` (but the method will)... Not a big deal I think

			for (field in annotation.fields) {
				val importableField = TemporaryABFField<KMutableProperty1<Any, Any>>()
				var modified = false
				// FIXME: This will probably not work when properties are NOT mutable
				clazz.kotlin.declaredMemberProperties.firstOrNull { it.name.equals(field) }?.apply { importableField.prop = this as KMutableProperty1<Any, Any>; modified = true }
				clazz.declaredFields.firstOrNull { it.name.equals(field) }?.apply { importableField.field = this; modified = true }
				clazz.declaredMethods.firstOrNull { val name = it.name; name == "get${field.capitalize()}" && !it.isVarArgs && it.parameterCount == 1 }?.apply { importableField.getMethod = this; modified = true }
				clazz.declaredMethods.firstOrNull { val name = it.name; name == "set${field.capitalize()}" && !it.isVarArgs && it.parameterCount == 1 }?.apply { importableField.setMethod = this; modified = true }

				if (!modified) {
					throw RuntimeException("Field '$field' not found on class '$clazz'")
				}
				val resultField = ImportableField(importableField.field, importableField.getMethod, importableField.setMethod, importableField.prop)
				result.add(resultField)
			}
		}
	}

	private val rootContext = SPValueMap(null, false)

	private val rootEndianness: ByteOrder

	init {
		val rootAnnotation = rootClass.getAnnotation(ABFObject::class.java) ?: throw ABFMissingAnnotationException(ABFObject::class, rootClass.name)
		if (rootAnnotation.endianness == ABFEndianness.INHERIT) throw RuntimeException("Root class '$rootClass' endianness must be defined (@${ABFObject::class.simpleName}.endianness should be set to other than ABFEndianness.INHERIT)")

		rootEndianness = rootAnnotation.endianness.nioOrder!!
	}

	fun import(bytes: ByteArray): T {
		val bb = ByteBuffer.wrap(bytes)

		bb.order(rootEndianness)

		setInitialProps(rootContext, bytes.size.toLong())
		return process(bb, rootClass, rootContext)
	}

	fun import(path: String): T {
		val f = File(path)
		if (!f.exists()) {
			throw RuntimeException("File '$path' doesn't exist")
		}
		if (!f.isFile) {
			throw RuntimeException("'$path' is not a valid file")
		}
		val bb = FileChannel.open(f.toPath()).map(FileChannel.MapMode.READ_ONLY, 0, f.length())
		bb.order(rootEndianness)

		setInitialProps(rootContext, f.length())
		return process(bb, rootClass, rootContext)
	}

	private fun setInitialProps(context: SPValueMap, size: Long) {
		context.setProp(PROP_LOCAL_OFFSET, SPValueLong(0))
		context.setProp(PROP_GLOBAL_OFFSET, SPValueLong(0))
		context.setProp(PROP_SIZE, SPValueLong(size))
	}

	private fun <K> newInstance(clazz: Class<K>): K {
		val constructor = clazz.declaredConstructors.firstOrNull { it.parameterCount == 0 } ?: throw RuntimeException("Class $clazz should have a no-arg constructor (it can be private)")
		val oldAccessible = constructor.isAccessible
		constructor.isAccessible = true
		val instance = constructor.newInstance() as K
		constructor.isAccessible = oldAccessible
		return instance
	}

	private fun <K> processObject(bb: ByteBuffer, obj: K, field: ImportableField, currentFieldContext: SPValueMap) {
		val newObj = process(bb, field.type, currentFieldContext)

		field.setValue(obj as Any, newObj)

		currentFieldContext.valid = true
	}

	private val arraysToProcess = mutableMapOf<ImportableField, SPValueContainer<*, Int>>()

	/**
	 * @return the size of the processed entity
	 */
	private fun <K> process(bb: ByteBuffer, clazz: Class<K>, currentContext: SPValueMap): K {
		val obj = newInstance(clazz)

		// Reset bytebuffer position
		bb.position((currentContext.getProp(ABFProcessor.PROP_GLOBAL_OFFSET) as SPValueLong).value.toInt())

		// Create context data first
		ABFContextProcessing.preprocess(clazz, arraysToProcess, currentContext)

		ABFContextProcessing.postProcess(currentContext)

		processArrayContexts(bb, currentContext)

		processFields(bb, obj, clazz, currentContext)

		var contextSizeProp = currentContext.getProp(PROP_SIZE) as SPValueLong?
		if (contextSizeProp == null) {
			contextSizeProp = SPValueLong(0, false)
			currentContext.setProp(PROP_SIZE, contextSizeProp)
		}
		if (!contextSizeProp.valid) {
			val importableFields = getImportableFields(clazz)
			val lastField = importableFields.last()
			val lastSizeProp = currentContext[lastField.name]!!.getProp(PROP_SIZE) as SPValueLong
			val lastLocalOffsetProp = currentContext[lastField.name]!!.getProp(PROP_LOCAL_OFFSET) as SPValueLong

			contextSizeProp.value = lastSizeProp.value + lastLocalOffsetProp.value
			contextSizeProp.valid = true
		}

		return obj
	}

	private fun processArrayContexts(bb: ByteBuffer, currentContext: SPValueMap) {
		if (arraysToProcess.isEmpty()) return

		val it = arraysToProcess.iterator()
		while (it.hasNext()) {
			val entry = it.next()
			try {
				val field = entry.key
				val currentFieldContext = currentContext[field.name] as SPValueContainer<*, *>?
				val globalOffsetProp = currentFieldContext?.getProp(PROP_GLOBAL_OFFSET) as SPValueLong?
				if (globalOffsetProp?.valid != true) continue

				bb.position(globalOffsetProp!!.value.toInt())

				val newLength = parseArrayLength(bb, field, currentContext).toInt()

				val type = field.type
				val elementSize:Int = when (type) {
					// TODO: Primitive arrays probably need to disappear
					BooleanArray::class.java -> {
						// TODO: Boolean can be of different sizes?
						(currentFieldContext as SPValueArrayBoolean).value = BooleanArray(newLength); 1
					}
					ByteArray::class.java -> {(currentFieldContext as SPValueArrayByte).value = ByteArray(newLength); 1}
					ShortArray::class.java -> {(currentFieldContext as SPValueArrayShort).value = ShortArray(newLength); 2}
					IntArray::class.java -> {(currentFieldContext as SPValueArrayInt).value = IntArray(newLength); 4}
					LongArray::class.java -> {(currentFieldContext as SPValueArrayLong).value = LongArray(newLength); 8}
					FloatArray::class.java -> {(currentFieldContext as SPValueArrayFloat).value = FloatArray(newLength); 4}
					DoubleArray::class.java -> {(currentFieldContext as SPValueArrayDouble).value = DoubleArray(newLength); 8}
					else -> {
						if (type.isArray || List::class.java.isAssignableFrom(type)) {
							(currentFieldContext as SPValueArray).value = Array(newLength) { j ->
								val result = SPValueMap(currentContext, false)
								if (j == 0) {
									result.setProp(PROP_GLOBAL_OFFSET, SPValueLong(globalOffsetProp.value))
									result.setProp(PROP_LOCAL_OFFSET, SPValueLong(0))
								} else {
									result.setProp(PROP_GLOBAL_OFFSET, SPValueLong(0, false))
									result.setProp(PROP_LOCAL_OFFSET, SPValueLong(0, false))
								}
								result.setProp(PROP_INDEX, SPValueLong(j.toLong()))
								result.setProp(PROP_SIZE, SPValueLong(0, false))
								result
							}
						}

						0
					}
				}

				if (elementSize > 0) {
					val sizeProp = currentFieldContext!!.getProp(PROP_SIZE) as SPValueLong?
					if (sizeProp == null) {
						currentFieldContext.setProp(PROP_SIZE, SPValueLong((newLength * elementSize).toLong()))
					} else {
						if (!sizeProp.valid) {
							sizeProp.value = (newLength * elementSize).toLong()
							sizeProp.valid = true
						}
					}
				}

				it.remove()
			} catch (e: Exception) {

			}
		}
	}

	fun <K> processFields(bb: ByteBuffer, obj: K, clazz: Class<K>, currentContext: SPValueMap) {
		var postProcessingChanged = false
		var incomplete = false

		val importableFields = getImportableFields(clazz)
		for (field in importableFields) {
			val currentFieldContext = currentContext[field.name] as SPValue

			if (currentFieldContext.valid == true) continue

			// TODO
			/*val exportCondition = field.getAnnotation(ExportCondition::class.java)
			if (exportCondition != null) {
				if (!parseExportCondition(exportCondition, obj)) continue
			}*/

			val dependencyCondition = field.getAnnotation(ABFImportDependency::class.java)
			if (dependencyCondition != null) {
				if (!dependenciesFullfilled(dependencyCondition, currentContext)) {
					incomplete = true
					continue
				}
			}

			// FIXME: Set valid bytebuffer position
			val globalOffset = currentFieldContext.getProp(ABFProcessor.PROP_GLOBAL_OFFSET) as SPValueLong
			if (!globalOffset.valid) {
				incomplete = true
				continue
			}

			bb.position(globalOffset.value.toInt())

			val type = field.type

//			val localOffset = currentBuf.position - initialOffset
			when (type) {
			// Primitives
			// FIXME: Boolean!
				Byte::class.java, java.lang.Byte::class.java -> processNumber(bb, obj, field, currentFieldContext as SPPrimitiveValue<Number>, ABFNumberType.UINT8)
				Short::class.java, java.lang.Short::class.java -> processNumber(bb, obj, field, currentFieldContext as SPPrimitiveValue<Number>, ABFNumberType.UINT16)
				Int::class.java, java.lang.Integer::class.java -> processNumber(bb, obj, field, currentFieldContext as SPPrimitiveValue<Number>, ABFNumberType.UINT32)
				Long::class.java, java.lang.Long::class.java -> processNumber(bb, obj, field, currentFieldContext as SPPrimitiveValue<Number>, ABFNumberType.UINT64)
				Float::class.java, java.lang.Float::class.java -> processNumber(bb, obj, field, currentFieldContext as SPPrimitiveValue<Number>, ABFNumberType.FLOAT)
				Double::class.java, java.lang.Double::class.java -> processNumber(bb, obj, field, currentFieldContext as SPPrimitiveValue<Number>, ABFNumberType.DOUBLE)
				String::class.java, java.lang.String::class.java -> processString(bb, obj, field, currentFieldContext as SPValueString)

			// Array of primitives
			// FIXME: Nullable arrays might not match this, fixed on the number side by importing `java.lang...`, but there is no class for Java arrays in Kotlin besides these
				BooleanArray::class.java -> TODO()
				ByteArray::class.java -> processByteArray(bb, obj, field, currentFieldContext as SPValueArrayByte, currentContext)
				ShortArray::class.java -> TODO()
				IntArray::class.java -> TODO()
				LongArray::class.java -> TODO()
				FloatArray::class.java -> TODO()
				DoubleArray::class.java -> TODO()

				ABFBitMask::class.java -> processBitmask(bb, obj, field, currentFieldContext as SPValueLong)
				ABFNumberValue::class.java -> processNumberValue(bb, obj, field, currentFieldContext as SPPrimitiveValue<Number>, currentContext)

				else -> {
					if (type.isEnum) {
						processEnum(bb, obj, field, currentFieldContext as SPPrimitiveValue<*>)
					} else if (type.isArray) {
						processArray(bb, obj, field, currentFieldContext as SPValueArray, currentContext)
					} else if (List::class.java.isAssignableFrom(type)) {
						TODO()
					} else {
						processObject(bb, obj, field, currentFieldContext as SPValueMap)
					}
				}
			}

			// Second phase
//			when (type) {
//				ABFNumberValue::class.java -> processNumberValueExpr(bb, obj, field, currentContext)
//			}

			// TODO: Only postprocess when we have something invalid?
			// FIXME: This can probably be at the beginning of the loop & we can remove the same methods on our caller
			if (ABFContextProcessing.postProcess(currentContext)) {
				postProcessingChanged = true
			}
			processArrayContexts(bb, currentContext)
		}

		if (incomplete) {
			if (!postProcessingChanged) {
				throw RuntimeException("Unfortunately, importing is not possible. Please check that all your importing conditions & expressions are valid (for example, check for circular dependencies between conditions)")
			} else {
				processFields(bb, obj, clazz, currentContext)
			}
		}
	}

	private fun dependenciesFullfilled(dependencyCondition: ABFImportDependency, currentContext: SPValueMap): Boolean {
		dependencyCondition.value.forEach { dependency ->
			val value = parse(dependency, currentContext)
			if (!value.valid) return false
		}
		return true
	}

	private fun <K> processNumberValue(bb: ByteBuffer, obj: K, field: ImportableField, context: SPPrimitiveValue<Number>, currentContext: SPValueMap) {
		val numberAnnotation = field.getAnnotation(ABFNumber::class.java) ?: throw ABFMissingAnnotationException(ABFNumber::class, field.name, "${ABFNumberValue::class.simpleName} needs it to specify the size to be imported")
		val value = field.getValue(obj as Any) as ABFNumberValue? ?: throw NullPointerException("${field.name} should have a value!")
		// TODO: We're reading the annotation again on processNumber, fix that
		processNumber(bb, numberAnnotation.type, field, context, numberAnnotation.type, setValue = false)
		value.importedValue = context.value

		val expr = value.importSPExpr ?: value.exportSPExpr
		val result = parse(expr, currentContext)
		if (result !is SPPrimitiveValue<*>) {
			throw RuntimeException("The result of a ABFNumberValue import / export SPExpr should be a numeric value (failed on \"$expr\" for field \"${field.name}\")")
		}
		if (result.valid && result.value != context.value) {
			throw RuntimeException("The result of a ABFNumberValue import / export SPExpr was different than a previously set value. This should not happen! (failed on \"$expr\" for field \"${field.name}\")")
		}
		(result as SPPrimitiveValue<Number>).value = context.value
		result.valid = true
	}

	/*private fun <K> processList(bb: ByteBuffer, obj: K, field: ImportableField, currentContext: SPValueMap): SPValueArray? {
		val annotation = field.getAnnotation(ABFImportObjectMaps::class.java) ?: throw ABFMissingAnnotationException(ABFImportObjectMaps::class, field.name, "List generic types get erased during compilation, so we need to know what to instantiate inside them.")

		val arraySize = parseArrayLength(bb, field, currentContext).toInt()

		// FIXME
		val arrayClass = field.type

		val array = Array<SPValue>(arraySize) { j ->
			val newContext = SPValueMap(currentContext)
//			val localOffset = currentBuf.position - initialOffset

			process(bb, arrayClass, newContext)

//			val newOffset = currentBuf.position
//			val size = newOffset - offset

			newContext.setProp(PROP_INDEX, SPValueLong(j.toLong()))
//			newContext.setProp(PROP_SIZE, SPValueLong(size.toLong()))
//			newContext.setProp(PROP_LOCAL_OFFSET, SPValueLong(localOffset.toLong()))
//			newContext.setProp(PROP_GLOBAL_OFFSET, SPValueLong(offset))

//			offset = newOffset

			newContext
		}
		return SPValueArray(array)
	}*/

	private fun <K> processArray(bb: ByteBuffer, obj: K, field: ImportableField, currentFieldContext: SPValueArray, currentContext: SPValueMap, setValue: Boolean = true) {
		val annotationObjectsMap = field.getAnnotation(ABFImportObjectMaps::class.java)

		val arraySize = parseArrayLength(bb, field, currentContext).toInt()

		// Array should have been filled by processArrayContexts. If not, we're in trouble!
		if (currentFieldContext.value.size != arraySize) {
			throw RuntimeException("Array should have been initialized by processArrayContexts!")
		}

		val parentGlobalOffsetProp = currentFieldContext.getProp(PROP_GLOBAL_OFFSET) as SPValueLong
		if (!parentGlobalOffsetProp.valid) {
			throw RuntimeException("Something is wrong!")
		}
		val parentGlobalOffset = parentGlobalOffsetProp.value
		var localOffset = 0L

		val array = java.lang.reflect.Array.newInstance(field.type.componentType, arraySize) as Array<Any>
		for (j in 0..arraySize - 1) {
			val newContext = currentFieldContext[j] as SPValueMap

			val globalOffsetProp = newContext.getProp(PROP_GLOBAL_OFFSET) as SPValueLong
			if (!globalOffsetProp.valid) {
				globalOffsetProp.value = parentGlobalOffset + localOffset
				globalOffsetProp.valid = true
			}
			val localOffsetProp = newContext.getProp(PROP_LOCAL_OFFSET) as SPValueLong
			if (!localOffsetProp.valid) {
				localOffsetProp.value = localOffset
				localOffsetProp.valid = true
			}

			val arrayClass = if (annotationObjectsMap == null) {
				field.type.componentType
			} else {
				val objectMap = annotationObjectsMap.value.firstOrNull {
					val spValue = parse(it.expr, newContext)
					if (spValue !is SPValueBoolean) {
						throw RuntimeException("Object map expression '${it.expr}' on field '${field.name}' returned a non-boolean value")
					}
					spValue.value
				} ?: throw RuntimeException("Couldn't find a valid class to instantiate while parsing ABFImportObjectMaps on field \"${field.name}\"")
				objectMap.clazz.java
			}

			val newObj = process(bb, arrayClass, newContext)

			newContext.valid = true

			localOffset += (newContext.getProp(PROP_SIZE) as SPValueLong).value

			array[j] = newObj
		}

		if (setValue) {
			field.setValue(obj as Any, array)
		}
		if (!currentFieldContext.getProp(PROP_SIZE)!!.valid) {
			currentFieldContext.setProp(PROP_SIZE, SPValueLong(localOffset))
		}

		currentFieldContext.valid = true
	}

	private fun <K> processByteArray(bb: ByteBuffer, obj: K, field: ImportableField, currentFieldContext: SPValueArrayByte, currentContext: SPValueMap) {
		val arraySize = parseArrayLength(bb, field, currentContext).toInt()

		val result = ByteArray(arraySize)
		bb.get(result)
		currentFieldContext.value = result
		currentFieldContext.valid = true
		field.setValue(obj as Any, result)
	}

	private fun parseArrayLength(bb: ByteBuffer, field: ImportableField, currentContext: SPValueMap): Number {
		val annotation = field.getAnnotation(ABFArray::class.java)
		val hasLength = annotation?.hasLength ?: true

		// TODO: Document this: When `hasLength` is provided, that's checked first. If not, @ABFImportArraySize will be checked if provided. If not, it will fail
		return if (hasLength) {
			val lengthType = annotation?.lengthType ?: ABFNumberType.UINT32
			readNumber(bb, lengthType)
		} else {
			val sizeAnnotation = field.getAnnotation(ABFImportArraySize::class.java) ?: throw UnsupportedOperationException("Arrays need to have sizes or @${ABFImportArraySize::class.simpleName} annotations to be imported")
			val spValue = parse(sizeAnnotation.value, currentContext)
			if (spValue !is SPValueLong) {
				throw RuntimeException("Expression '${sizeAnnotation.value}' did not return a number")
			}
			if (!spValue.valid) {
				throw RuntimeException("Invalid length value for field \"$field\"")
			}
			spValue.value
		}
	}

	private fun <K> processBitmask(bb: ByteBuffer, obj: K, field: ImportableField, currentFieldContext: SPValueLong) {
		val numberAnnotation = field.getAnnotation(ABFNumber::class.java) ?: throw RuntimeException("Bit masks need an @${ABFNumber::class.simpleName} annotation to specify the size to be imported")
		// FIXME: Invalid number type if it's DOUBLE/FLOAT or signed
		val lengthType = numberAnnotation.type
		val value = readNumber(bb, lengthType).toLong()

		val originalBitmaskObj = field.getValue(obj as Any) as ABFBitMask

		// TODO: We use the size of the bitmask present on the object itself so when we compare both objects they're equal. We should document this!
		val bits = BooleanArray(originalBitmaskObj.size)
		var bitmask = 1L
		for (j in 0..bits.size - 1) {
			if (bitmask and value != 0L) {
				bits[j] = true
			}
			bitmask = bitmask shl 1
		}
		val bitmaskObj = ABFBitMask(Array(bits.size) { j -> { -> bits[j] } })

		/*
		 * FIXME:
		 * We're replacing the value, which means the original object won't work anymore if we try to export it again
		 * because the original ABFBitMask had references to functions that we're removing!
		 */
		field.setValue(obj, bitmaskObj)

		currentFieldContext.value = value
		currentFieldContext.setProp(PROP_BITS, SPValueArrayBoolean(bits))
		currentFieldContext.valid = true
	}

	private fun <K> processEnum(bb: ByteBuffer, obj: K, field: ImportableField, context: SPPrimitiveValue<*>) {
		val annotation = field.getAnnotation(ABFEnum::class.java)
		// FIXME: Get default values from annotation itself
		val exportType = annotation?.enumType ?: ABFEnumType.ORDINAL

		when (exportType) {
			ABFEnumType.NAME -> {
				processString(bb, null, field, context as SPValueString, setValue = false)
				for (enumConstant in field.type.enumConstants) {
					if ((enumConstant as Enum<*>).name == context.value) {
						field.setValue(obj as Any, enumConstant)
						break
					}
				}
			}
			ABFEnumType.TOSTRING -> {
				processString(bb, null, field, context as SPValueString, setValue = false)
				for (enumConstant in field.type.enumConstants) {
					if ((enumConstant as Enum<*>).toString() == context.value) {
						field.setValue(obj as Any, enumConstant)
						break
					}
				}
			}
			ABFEnumType.ORDINAL -> {
				val ordinalType = annotation?.ordinalType ?: ABFNumberType.UINT32
				processNumber(bb, null, field, context as SPPrimitiveValue<Number>, ordinalType, setValue = false)
				field.setValue(obj as Any, field.type.enumConstants[(context as SPPrimitiveValue<Number>).value.toInt()])
			}
		}
	}

	private fun <K> processNumber(bb: ByteBuffer, obj: K, field: ImportableField, context: SPPrimitiveValue<in Number>, defaultType: ABFNumberType, setValue: Boolean = true) {
		val annotation = field.getAnnotation(ABFNumber::class.java)
		val type = annotation?.type ?: defaultType
		val value = readNumber(bb, type)
		if (setValue) {
			field.setValue(obj as Any, value)
		}

		context.value = value
		context.valid = true
	}

	private fun <K> processString(bb: ByteBuffer, obj: K, field: ImportableField, currentFieldContext: SPValueString, setValue: Boolean = true) {
		val annotationFixed = field.getAnnotation(ABFFixedString::class.java)

		currentFieldContext.value = if (annotationFixed != null) {
			val str = readFixedString(annotationFixed, bb)

			if (setValue) {
				field.setValue(obj as Any, str)
			}

			str
		} else {
			// Variable size string
			val annotation = field.getAnnotation(ABFString::class.java)

			val alignment = annotation?.alignment ?: 0
			val zeroPadded = annotation?.zeroPadded ?: false
			val exportLength = annotation?.hasLength ?: false
			val lengthType = annotation?.lengthType ?: ABFNumberType.UINT8

			val str = readVariableString(bb, alignment, zeroPadded, exportLength, lengthType, currentFieldContext)

			if (setValue) {
				field.setValue(obj as Any, str)
			}

			str
		}
		currentFieldContext.valid = true
	}

	private fun readFixedString(annotationFixed: ABFFixedString, bb: ByteBuffer): String {
		// Fixed string
		val buf = ByteArray(annotationFixed.size)
		bb.get(buf)
		var zeroIdx = buf.indexOf(0)
		if (zeroIdx < 0) {
			zeroIdx = buf.size
		}

		return String(buf, 0, zeroIdx)
	}

	private fun readVariableString(bb: ByteBuffer, alignment: Int, zeroPadded: Boolean, hasLength: Boolean, lengthType: ABFNumberType, currentFieldContext: SPValueString): String {
		if (zeroPadded) {
			throw NotImplementedError()
		} else {
			if (hasLength) {
				val initialPos = bb.position()
				val size = readNumber(bb, lengthType).toInt()
				val buf = ByteArray(size)
				bb.get(buf)
				val padding = if (alignment > 0) {
					val mod = size % alignment
					if (mod > 0) alignment - mod else 0
				} else {
					0
				}

				currentFieldContext.setProp(PROP_SIZE, SPValueLong((bb.position() - initialPos + padding).toLong()))

				return String(buf)
			} else {
				throw NotImplementedError()
			}
		}
	}
}