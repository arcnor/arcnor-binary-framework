/**
 * Copyright 2016 Edu Garcia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arcnor.binary.common

import com.arcnor.sp.data.SPValue
import com.arcnor.sp.data.SPValueMap
import com.arcnor.sp.parser.SPLexer
import com.arcnor.sp.parser.SPParser
import com.arcnor.sp.parser.SPParserListener
import org.antlr.v4.runtime.ANTLRInputStream
import org.antlr.v4.runtime.CommonTokenStream
import java.lang.reflect.Field
import java.lang.reflect.Method
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.KProperty
import kotlin.reflect.KProperty1
import kotlin.reflect.jvm.isAccessible
import kotlin.reflect.jvm.javaField

abstract class ABFProcessor {
	companion object {
		const val PROP_INDEX = "index"
		const val PROP_SIZE = "size"
		const val PROP_LOCAL_OFFSET = "localOffset"
		const val PROP_GLOBAL_OFFSET = "globalOffset"
		const val PROP_BITS = "bits"

		internal fun parse(expr: String, context: SPValueMap? = null): SPValue {
			val lexer = SPLexer(ANTLRInputStream(expr))
			val tokens = CommonTokenStream(lexer)
			val p = SPParser(tokens)
			p.buildParseTree = true
			val listener = SPParserListener(context ?: SPValueMap(null))
			p.addParseListener(listener)

			p.expr()
			return listener.result()
		}

		internal fun <T : Annotation> getAnnotation(annotationClass: Class<T>, clazz: Class<*>): T? {
			var clazz = clazz
			while (clazz != Object::class.java) {
				val annotation = clazz.getAnnotation(annotationClass)
				if (annotation != null) {
					return annotation
				}
				clazz = clazz.superclass as Class<Any>
			}
			return null
		}
	}

	protected data class TemporaryABFField<K : KProperty1<Any, Any>>(
			var field: Field? = null,
			var getMethod: Method? = null,
			var setMethod: Method? = null,
			var prop: K? = null)

	protected class ABFField() {

	}

	internal class ImportableField(
			private val field: Field?,
			private val getMethod: Method?,
			private val setMethod: Method?,
			private val prop: KMutableProperty1<Any, Any>?
	) {
		val type: Class<*>
			get() = prop?.javaField?.type
					?: this.field?.type
					?: getMethod?.returnType
					?: setMethod?.parameterTypes?.get(0)
					?: throw RuntimeException("Not a valid field!")

		private fun <R> KProperty<R>.isAnnotationPresent(clazz: Class<*>): Boolean {
			for (annotation in annotations) {
				if (annotation.annotationClass.java == clazz)
					return true
			}
			return false
		}

		private fun <R, T> KProperty<R>.getAnnotation(clazz: Class<T>): T? {
			for (annotation in annotations) {
				if (annotation.annotationClass.java == clazz)
					return annotation as T
			}
			return null
		}

		fun <T : Annotation> isAnnotationPresent(annotationClass: Class<T>): Boolean
				= wrapAccessibleProp { prop -> prop.isAnnotationPresent(annotationClass) }
				?: wrapAccessibleField { field -> field.isAnnotationPresent(annotationClass) }
				?: getMethod?.isAnnotationPresent(annotationClass)
				?: false

		fun <T : Annotation> getAnnotation(annotationClass: Class<T>)
				= wrapAccessibleProp { prop -> prop.getAnnotation(annotationClass) }
				?: wrapAccessibleField { field -> field.getAnnotation(annotationClass) }
				?: getMethod?.getAnnotation(annotationClass)

		fun getValue(obj: Any): Any?
				= wrapAccessibleProp { prop -> prop.get(obj) }
				?: wrapAccessibleField { field -> field.get(obj) }
				?: getMethod?.invoke(obj)

		fun setValue(obj: Any, value: Any)
				= wrapAccessibleProp { prop -> prop.set(obj, value) }
				?: wrapAccessibleField { field -> field.set(obj, value) }
				?: setMethod?.invoke(obj)

		private fun <T> wrapAccessibleProp(callback: (prop: KMutableProperty1<Any, Any>) -> T?): T? {
			if (prop == null) return null
			val previousAccessibility = prop.isAccessible

			prop.isAccessible = true
			val result = callback(prop)
			prop.isAccessible = previousAccessibility

			return result
		}

		private fun <T> wrapAccessibleField(callback: (field: Field) -> T?): T? {
			if (field == null) return null
			val previousAccessibility = field.isAccessible

			field.isAccessible = true
			val result = callback(field)
			field.isAccessible = previousAccessibility

			return result
		}

		val name: String
			get() = prop?.name
					?: this.field?.name
					?: getMethod?.name
					?: throw RuntimeException("Not a valid field!")

		override fun toString(): String{
			return "ImportableField($name)"
		}
	}

	internal class ExportableField(
			private val field: Field?,
			private val method: Method?,
			private val prop: KProperty1<Any, *>?
	) {
		val type: Class<*>
			get() = prop?.javaField?.type
					?: this.field?.type
					?: method?.returnType
					?: throw RuntimeException("Not a valid field!")

		private fun <R> KProperty<R>.isAnnotationPresent(clazz: Class<*>): Boolean {
			for (annotation in annotations) {
				if (annotation.annotationClass.java == clazz)
					return true
			}
			return false
		}

		private fun <R, T> KProperty<R>.getAnnotation(clazz: Class<T>): T? {
			for (annotation in annotations) {
				if (annotation.annotationClass.java == clazz)
					return annotation as T
			}
			return null
		}

		fun <T : Annotation> isAnnotationPresent(annotationClass: Class<T>): Boolean
				= wrapAccessibleProp { prop -> prop.isAnnotationPresent(annotationClass) }
				?: wrapAccessibleField { field -> field.isAnnotationPresent(annotationClass) }
				?: method?.isAnnotationPresent(annotationClass)
				?: false

		fun <T : Annotation> getAnnotation(annotationClass: Class<T>)
				= wrapAccessibleProp { prop -> prop.getAnnotation(annotationClass) }
				?: wrapAccessibleField { field -> field.getAnnotation(annotationClass) }
				?: method?.getAnnotation(annotationClass)

		fun getValue(obj: Any)
				= wrapAccessibleProp { prop -> prop.get(obj) }
				?: wrapAccessibleField { field -> field.get(obj) }
				?: method?.invoke(obj)

		private fun <T> wrapAccessibleProp(callback: (prop: KProperty1<Any, *>) -> T?): T? {
			if (prop == null) return null
			val previousAccessibility = prop.isAccessible

			prop.isAccessible = true
			val result = callback(prop)
			prop.isAccessible = previousAccessibility

			return result
		}

		private fun <T> wrapAccessibleField(callback: (field: Field) -> T?): T? {
			if (field == null) return null
			val previousAccessibility = field.isAccessible

			field.isAccessible = true
			val result = callback(field)
			field.isAccessible = previousAccessibility

			return result
		}

		val name: String
			get() = prop?.name
					?: this.field?.name
					?: method?.name
					?: throw RuntimeException("Not a valid field!")
	}

	// Reflection helpers //

	protected fun getProperty(name: String, memberProperties: Collection<KProperty1<Any, *>>): KProperty1<Any, *>? {
		return memberProperties.firstOrNull { it.name.equals(name) }
	}
}