/**
 * Copyright 2016 Edu Garcia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arcnor.binary.common

/**
 * A bitmask that can be exported/imported
 */
// TODO: Convert internal data to BitSet
class ABFBitMask(private val value: Array<() -> Boolean>) : Iterable<Boolean> {
	override fun iterator() = object : Iterator<Boolean> {
		private var index = 0;

		override fun hasNext() = index < size
		override fun next() = get(index++)
	}

	fun get(index: Int) = value[index]()
	val size: Int
		get() = value.size

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (other?.javaClass != javaClass) return false

		other as ABFBitMask

		if (value.size != other.size) return false

		for (j in 0..value.size - 1) {
			val f1 = value[j]
			val f2 = other.value[j]

			if (f1() != f2()) {
				return false
			}
		}

		return true
	}

	override fun hashCode(): Int{
		var result = 0
		value.forEach { result += 31 * result + it.hashCode() }
		return result
	}
}
