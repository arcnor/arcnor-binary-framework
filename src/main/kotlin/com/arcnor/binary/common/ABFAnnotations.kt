/**
 * Copyright 2016 Edu Garcia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arcnor.binary.common

import com.arcnor.binary.ABFEndianness
import com.arcnor.binary.ABFEnumType
import com.arcnor.binary.ABFNumberType
import kotlin.reflect.KClass

@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class ABFObject(
		// TODO: Deprecate, same functionality can be achieved with SP expressions?
		/**
		 * Object length
		 *
		 * When exporting: if true, the length of this object will be added to the beginning of the object
		 *
		 * When importing: if true, the length will be used to verify that the object has the right length
		 */
		val hasLength: Boolean = false,
		val lengthType: ABFNumberType = ABFNumberType.UINT32,
		/**
		 * Endianness of this object. [ABFEndianness].INHERIT cannot be used on the root object
		 */
		val endianness: ABFEndianness = ABFEndianness.INHERIT,
		val fields: Array<String>
)

@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FIELD, AnnotationTarget.PROPERTY, AnnotationTarget.PROPERTY_GETTER)
@Repeatable
annotation class ABFImportObjectMaps(
		val value: Array<ABFImportObjectMap>
)

@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.EXPRESSION, AnnotationTarget.FIELD, AnnotationTarget.PROPERTY, AnnotationTarget.PROPERTY_GETTER)
@Repeatable
annotation class ABFImportObjectMap(
		val expr: String,
        val clazz: KClass<*>
)

@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FIELD, AnnotationTarget.PROPERTY, AnnotationTarget.PROPERTY_GETTER)
annotation class ABFImportArraySize(
		/**
		 * SP expression that evaluates to a number, used to create an array of that size
		 */
		val value: String
)

@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FIELD, AnnotationTarget.PROPERTY, AnnotationTarget.PROPERTY_GETTER)
annotation class ABFNumber(
		val type: ABFNumberType
)

@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FIELD, AnnotationTarget.PROPERTY, AnnotationTarget.PROPERTY_GETTER)
annotation class ABFArray(
		/**
		 * Array length
		 *
		 * When exporting: if true, the length of this object will be added to the beginning of the object
		 *
		 * When importing: if true, the length will be used to create the array and fill it with `length` elements
		 */
		val hasLength: Boolean = true,
		val lengthType: ABFNumberType = ABFNumberType.UINT32
)

@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FIELD, AnnotationTarget.PROPERTY, AnnotationTarget.PROPERTY_GETTER)
/**
 * Default annotation for strings
 */
annotation class ABFString(
		val hasLength: Boolean = false,
		val lengthType: ABFNumberType = ABFNumberType.UINT8,
		/**
		 * If true, a zero (`\u0000`) character will be added at the end
		 */
		val zeroPadded: Boolean = false,
		/**
		 * If bigger than 0, zeroes will be added to the end to make the written/read data a multiple of the alignment value.
		 * `zeroPadded` can still be used to make sure there is always a 0 at the end of the written data.
		 */
		val alignment: Int = 0
)

@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FIELD, AnnotationTarget.PROPERTY, AnnotationTarget.PROPERTY_GETTER)
annotation class ABFFixedString(
		/**
		 * When exporting: the string will be truncated & filled with zeroes to fill this size
		 *
		 * When importing: `size` bytes will be read to create this string, and all trailing zeroes will be discarded
		 */
		val size: Int
)

@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FIELD, AnnotationTarget.PROPERTY, AnnotationTarget.PROPERTY_GETTER)
annotation class ABFEnum(
		// TODO: When enumType is NAME or TOSTRING, we expect @ABFString to be there to control the output. We should do the same with @ABFNumber, ABFEnumType.ORDINAL & ordinalType
		/**
		 * When `enumType` is [ABFEnumType.ORDINAL], this property specifies the length of the number to be exported/imported
		 */
		val ordinalType: ABFNumberType = ABFNumberType.UINT8,
		val enumType: ABFEnumType = ABFEnumType.ORDINAL
)
