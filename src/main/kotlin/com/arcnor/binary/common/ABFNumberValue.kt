/**
 * Copyright 2016 Edu Garcia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arcnor.binary.common

/**
 * Exports/Imports a number from an SP expression
 *
 * @param exportSPExpr the expression to be used for exporting
 * @param importSPExpr the expression to be used for importing. If not provided, [exportSPExpr] will be used instead.
 * This can be used to set a property (NOT a value) of our current OR parent context.
 */
class ABFNumberValue(val exportSPExpr: String, val importSPExpr: String? = null) {
	var importedValue: Number = 0

	override fun equals(other: Any?): Boolean{
		if (this === other) return true
		if (other?.javaClass != javaClass) return false

		other as ABFNumberValue

		if (exportSPExpr != other.exportSPExpr) return false
		if (importSPExpr != other.importSPExpr) return false

		return true
	}

	override fun hashCode(): Int{
		var result = exportSPExpr.hashCode()
		result = 31 * result + (importSPExpr?.hashCode() ?: 0)
		return result
	}
}
