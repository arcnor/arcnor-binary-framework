/**
 * Copyright 2016 Edu Garcia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arcnor.binary.import

import com.arcnor.binary.ABFEndianness
import com.arcnor.binary.ABFNumberType.SINT16
import com.arcnor.binary.ABFNumberType.UINT16
import com.arcnor.binary.ABFNumberType.UINT32
import com.arcnor.binary.common.ABFArray
import com.arcnor.binary.common.ABFBitMask
import com.arcnor.binary.common.ABFEnum
import com.arcnor.binary.common.ABFFixedString
import com.arcnor.binary.common.ABFImportArraySize
import com.arcnor.binary.common.ABFImportObjectMap
import com.arcnor.binary.common.ABFImportObjectMaps
import com.arcnor.binary.common.ABFNumber
import com.arcnor.binary.common.ABFNumberValue
import com.arcnor.binary.common.ABFObject
import java.util.Arrays

@ABFObject(endianness = ABFEndianness.LITTLE, fields = arrayOf("magic", "numLumps", "directoryOffset", "lumpData", "lumpHeaders"))
class WADFile(@ABFArray(hasLength = false)
              @ABFImportArraySize("\$numLumps")
              @ABFImportObjectMaps(arrayOf(
		              ABFImportObjectMap("\$this#index == 0", WADLumpNoData::class),
		              ABFImportObjectMap("\$lumpHeaders[\$this#index].name == \"THINGS\"", WADLumpTHINGS::class)
              ))
              @ABFImportDependency(arrayOf("\$numLumps", "\$lumpHeaders"))
              var lumpData: Array<WADLump>,

              @ABFArray(hasLength = false)
              @ABFImportArraySize("\$numLumps")
              @ABFImportDependency(arrayOf("\$numLumps"))
              var lumpHeaders: Array<WADLumpHeader>
) {
	private constructor() : this(emptyArray<WADLump>(), emptyArray<WADLumpHeader>()) {
	}

	@ABFFixedString(4)
	var magic = "PWAD"
	var numLumps = lumpHeaders.size
	// TODO: This should be export/importNumberValue, so we set "$lumpHeaders#globalOffset" to the number when importing
	@ABFNumber(UINT32)
	var directoryOffset = ABFNumberValue("\$lumpHeaders#globalOffset")

	init {
		// FIXME: Convert into a check that can be used for Imports & Exports
		if (lumpData.size != lumpHeaders.size) {
			throw RuntimeException("There has to be the same number of lump & headers")
		}
		/*if (lumpHeaders.size < 1) {
			throw RuntimeException("There has to be at least one lump defined")
		}
		if (lumpHeaders[0] !is WADLumpHeaderMapName) {
			throw RuntimeException("First lump should be the map name")
		}*/
	}

	override fun equals(other: Any?): Boolean{
		if (this === other) return true
		if (other?.javaClass != javaClass) return false

		other as WADFile

		if (!Arrays.equals(lumpData, other.lumpData)) return false
		if (!Arrays.equals(lumpHeaders, other.lumpHeaders)) return false
		if (magic != other.magic) return false
		if (numLumps != other.numLumps) return false
		if (directoryOffset != other.directoryOffset) return false

		return true
	}

	override fun hashCode(): Int{
		var result = Arrays.hashCode(lumpData)
		result = 31 * result + Arrays.hashCode(lumpHeaders)
		result = 31 * result + magic.hashCode()
		result = 31 * result + numLumps
		result = 31 * result + directoryOffset.hashCode()
		return result
	}
}

@ABFObject(fields = arrayOf())
abstract class WADLump

@ABFObject(fields = arrayOf())
class WADLumpNoData : WADLump() {
	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (other?.javaClass != javaClass) return false

		return true
	}

	override fun hashCode(): Int {
		return super.hashCode()
	}
}

@ABFObject(fields = arrayOf("offset", "lumpSize", "name"))
open class WADLumpHeader() {
	@ABFNumber(UINT32)
	var offset = ABFNumberValue("\$lumpData[\$this#index]#globalOffset")
	@ABFNumber(UINT32)
	var lumpSize = ABFNumberValue("\$lumpData[\$this#index]#size")
	@ABFFixedString(8)
	open var name: String = ""

	override fun equals(other: Any?): Boolean{
		if (this === other) return true
		if (other !is WADLumpHeader) return false

		if (offset != other.offset) return false
		if (lumpSize != other.lumpSize) return false
		if (name != other.name) return false

		return true
	}

	override fun hashCode(): Int{
		var result = offset.hashCode()
		result = 31 * result + lumpSize.hashCode()
		result = 31 * result + name.hashCode()
		return result
	}
}

@ABFObject(fields = arrayOf())
class WADLumpHeaderMapName(override var name: String) : WADLumpHeader() {
	private constructor() : this("")
}

@ABFObject(fields = arrayOf("values"))
class WADLumpTHINGS(@ABFArray(hasLength = false)
                    @ABFImportArraySize("\$this#size / 10")
                    var values: Array<WADThing>) : WADLump() {
	private constructor() : this(emptyArray())

	override fun equals(other: Any?): Boolean{
		if (this === other) return true
		if (other?.javaClass != javaClass) return false

		other as WADLumpTHINGS

		if (!Arrays.equals(values, other.values)) return false

		return true
	}

	override fun hashCode(): Int{
		return Arrays.hashCode(values)
	}
}

@ABFObject(fields = arrayOf())
class WADLumpHeaderTHINGS() : WADLumpHeader() {
	override var name = "THINGS"
}

@ABFObject(fields = arrayOf("x", "y", "angle", "type", "spawnFlags"))
class WADThing(
		@ABFNumber(SINT16)
		var x: Int,
		@ABFNumber(SINT16)
		var y: Int,
		@ABFNumber(com.arcnor.binary.ABFNumberType.UINT16)
		var angle: Int,
		@ABFEnum(ordinalType = com.arcnor.binary.ABFNumberType.UINT16, enumType = com.arcnor.binary.ABFEnumType.ORDINAL)
		var type: WADThingType,
		private val spawnedOnEasy: Boolean,
		private val spawnedOnMedium: Boolean,
		private val spawnedOnHard: Boolean) {
	private constructor() : this(0, 0, 0, WADThingType.UNUSED, false, false, false)

	@ABFNumber(UINT16)
	var spawnFlags = ABFBitMask(arrayOf(
			{ spawnedOnEasy }, { spawnedOnMedium }, { spawnedOnHard }
	))

	override fun equals(other: Any?): Boolean{
		if (this === other) return true
		if (other?.javaClass != javaClass) return false

		other as WADThing

		if (x != other.x) return false
		if (y != other.y) return false
		if (angle != other.angle) return false
		if (type != other.type) return false
		if (spawnFlags != other.spawnFlags) return false

		return true
	}

	override fun hashCode(): Int{
		var result = x
		result = 31 * result + y
		result = 31 * result + angle
		result = 31 * result + type.hashCode()
		result = 31 * result + spawnFlags.hashCode()
		return result
	}
}

enum class WADThingType {
	UNUSED,
	ZDOOM_POS_START_1,
	ZDOOM_POS_START_2,
	ZDOOM_POS_START_3,
	ZDOOM_POS_START_4,
	DOOM_BLUE_CARD,
	DOOM_YELLOW_CARD,
	DOOM_SPIDER_MASTERMIND,
	DOOM_BACKPACK,
	DOOM_SHOTGUN_GUY
}
