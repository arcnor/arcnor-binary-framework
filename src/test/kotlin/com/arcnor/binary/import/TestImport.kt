/**
 * Copyright 2016 Edu Garcia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arcnor.binary.import

import com.arcnor.binary.common.TestString
import com.arcnor.binary.common.TestStringAlignment
import com.arcnor.binary.export.ABFExport
import org.junit.Assert
import org.junit.Test
import java.io.ByteArrayOutputStream

class TestImport {
	// TODO: Test import of sint / uint numbers (a SINT16 on an Int field will probably fail right now)
	@Test
	fun testString() {
		val obj1 = TestString("abcde", 16)
		doTest("\u0005abcde\u0010".toByteArray(), obj1, TestString::class.java)
	}

	@Test
	fun testStringAlignment() {
		val obj1 = TestStringAlignment("abcd", 16)
		doTest("\u0004abcd\u0010".toByteArray(), obj1, TestStringAlignment::class.java)
		val obj2 = TestStringAlignment("abcde", 16)
		doTest("\u0005abcde\u0000\u0000\u0000\u0010".toByteArray(), obj2, TestStringAlignment::class.java)
	}

	private fun <T> doTest(bytes: ByteArray, expected: T, clazz: Class<T>) {
		val newObj = ABFImport(clazz).import(bytes)
		Assert.assertEquals(expected, newObj)
	}

	@Test
	fun testImport() {
		val lumpData = arrayOf(
				WADLumpNoData(),
				WADLumpTHINGS(
						arrayOf(
								WADThing(-66, -52, 0, WADThingType.ZDOOM_POS_START_1, spawnedOnEasy = true, spawnedOnMedium = true, spawnedOnHard = true),
								WADThing(768, -128, 180, WADThingType.DOOM_SPIDER_MASTERMIND, spawnedOnEasy = true, spawnedOnMedium = true, spawnedOnHard = true)
						)
				)
		)
		val lumpHeaders = arrayOf(
				WADLumpHeaderMapName("E2M1"), WADLumpHeaderTHINGS()
		)
		val testFile = WADFile(lumpData, lumpHeaders)

		val exporter = ABFExport(testFile)
		val baos = ByteArrayOutputStream()
		exporter.export(baos)

		val importer = ABFImport(WADFile::class.java)
		val newFile = importer.import(baos.toByteArray())

		Assert.assertEquals(testFile, newFile)
	}
}