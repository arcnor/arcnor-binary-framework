/**
 * Copyright 2016 Edu Garcia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arcnor.binary.util

import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import java.nio.ByteOrder

class TestDynamicByteBuffer {
	@Test
	fun testBufferGrows() {
		val dyn = DynamicByteBuffer(4)
		dyn.writeInt16(42)
		dyn.writeInt16(43)
		dyn.writeInt16(44)
		dyn.writeInt16(45)
		dyn.writeInt16(46)

		dyn.flip()
		assertEquals(42, dyn.readUInt16())
		assertEquals(43, dyn.readUInt16())
		assertEquals(44, dyn.readUInt16())
		assertEquals(45, dyn.readUInt16())
		assertEquals(46, dyn.readUInt16())
	}

	@Test
	fun testReplace() {
		val dyn = DynamicByteBuffer(4)
		dyn.writeInt16(42)
		dyn.writeInt16(43)
		dyn.writeInt16(44)
		dyn.writeInt16(45)
		dyn.writeInt16(46)

		dyn.replaceInt16(0, 128)
		dyn.replaceInt16(4, 200)
		dyn.replaceInt16(2, 190)

		dyn.flip()
		assertEquals(128, dyn.readUInt16())
		assertEquals(190, dyn.readUInt16())
		assertEquals(200, dyn.readUInt16())
		assertEquals(45, dyn.readUInt16())
		assertEquals(46, dyn.readUInt16())
	}

	@Test
	fun testReplace2() {
		val dyn = DynamicByteBuffer(4)
		dyn.writeInt16(42)
		dyn.writeInt32(43)
		dyn.writeInt32(44)
		dyn.writeInt32(45)
		dyn.writeInt32(46)

		dyn.replaceInt16(0, 128)
		dyn.replaceInt32(6, 200)
		dyn.replaceInt32(2, 190)

		dyn.flip()
		assertEquals(128, dyn.readUInt16())
		assertEquals(190, dyn.readUInt32())
		assertEquals(200, dyn.readUInt32())
		assertEquals(45, dyn.readUInt32())
		assertEquals(46, dyn.readUInt32())
	}

	@Test
	fun testInt24Big() {
		val dyn = DynamicByteBuffer(4)
		dyn.order = ByteOrder.BIG_ENDIAN
		dyn.writeInt24(0x8090AB)

		dyn.flip()
		dyn.order = ByteOrder.BIG_ENDIAN
		assertEquals(0x80.toByte(), dyn.readSInt8())
		assertEquals(0x90.toByte(), dyn.readSInt8())
		assertEquals(0xAB.toByte(), dyn.readSInt8())
	}

	@Test
	fun testInt24Small() {
		val dyn = DynamicByteBuffer(4)
		dyn.order = ByteOrder.LITTLE_ENDIAN
		dyn.writeInt24(0x8090AB)

		dyn.flip()
		dyn.order = ByteOrder.LITTLE_ENDIAN
		assertEquals(0xAB.toByte(), dyn.readSInt8())
		assertEquals(0x90.toByte(), dyn.readSInt8())
		assertEquals(0x80.toByte(), dyn.readSInt8())
	}

	@Test
	fun testPartialBuffers() {
		val dyn = DynamicByteBuffer(4)
		dyn.order = ByteOrder.BIG_ENDIAN
		dyn.writeInt8(1)
		dyn.writeInt8(2)
		dyn.writeInt64(3)

		dyn.flip()
		assertEquals(1.toByte(), dyn.readSInt8())
		assertEquals(2.toByte(), dyn.readSInt8())
		for (i in 0..6) {
			assertEquals(0.toByte(), dyn.readSInt8())
		}
		assertEquals(3.toByte(), dyn.readSInt8())
	}

	@Test
	fun testFirstBufferCanBeEmpty() {
		val dyn = DynamicByteBuffer(4)
		dyn.writeInt64(8L)

		dyn.flip()
		assertEquals(8, dyn.readSInt64())
	}

	@Test
	fun testByteOrder() {
		val dyn = DynamicByteBuffer(4)
		// Default should be LITTLE_ENDIAN
		dyn.writeInt16(8)
		dyn.order = ByteOrder.BIG_ENDIAN
		dyn.writeInt64(8)
		dyn.order = ByteOrder.LITTLE_ENDIAN
		dyn.writeInt64(8)
		dyn.order = ByteOrder.BIG_ENDIAN
		dyn.writeInt16(8)

		dyn.flip()
		// Little endian
		assertEquals(8, dyn.readUInt8())
		assertEquals(0, dyn.readUInt8())
		// Big endian
		for (i in 0..6) {
			assertEquals(0.toByte(), dyn.readSInt8())
		}
		assertEquals(8, dyn.readUInt8())
		// Little endian
		assertEquals(8, dyn.readUInt8())
		for (i in 0..6) {
			assertEquals(0.toByte(), dyn.readSInt8())
		}
		// Big endian
		assertEquals(0, dyn.readUInt8())
		assertEquals(8, dyn.readUInt8())
	}

	@Test
	fun testSigned() {
		val dyn = DynamicByteBuffer(4)
		dyn.writeInt8(-42)
		dyn.writeInt16(-43)
		dyn.writeInt32(-44)
		dyn.writeInt64(-45)

		dyn.flip()
		assertEquals((-42).toByte(), dyn.readSInt8())
		assertEquals((-43).toShort(), dyn.readSInt16())
		assertEquals(-44, dyn.readSInt32())
		assertEquals(-45L, dyn.readSInt64())
	}

	@Test
	fun testUnsigned() {
		val dyn = DynamicByteBuffer(4)
		dyn.writeInt8(0xE0)
		dyn.writeInt16(0xFF00)
		dyn.writeInt32(0xFFFFFF00.toInt())

		dyn.flip()
		assertEquals(0xE0, dyn.readUInt8())
		assertEquals(0xFF00, dyn.readUInt16())
		assertEquals(0xFFFFFF00L, dyn.readUInt32())
	}

	@Test
	fun testString() {
		val dyn = DynamicByteBuffer(4)
		// Byte ordering should not matter
		dyn.order = ByteOrder.BIG_ENDIAN
		dyn.writeString("My")
		dyn.order = ByteOrder.LITTLE_ENDIAN
		dyn.writeString("string")

		dyn.flip()
		assertEquals("My", dyn.readString(2))
		assertEquals("string", dyn.readString(6))
	}

	@Test
	fun testDeepCopySameInitialSize() {
		val dyn = DynamicByteBuffer(4)
		dyn.writeInt16(42)
		dyn.writeInt16(1985)
		dyn.writeInt16(99)
		dyn.writeInt16(2016)
		dyn.writeString("Lucas")

		val dyn2 = DynamicByteBuffer(4)
		dyn2.deepCopy(dyn)
		assertTrue("Original buffer should be valid after deep copy", dyn.isValid())
		assertEquals("Partial position is invalid", 13, dyn2.position)

		dyn2.flip()
		assertEquals(42, dyn2.readUInt16())
		assertEquals(1985, dyn2.readUInt16())
		assertEquals(99, dyn2.readUInt16())
		assertEquals(2016, dyn2.readUInt16())
		assertEquals("Lucas", dyn2.readString(5))
	}

	@Test
	fun testDeepCopyDifferentInitialSize() {
		val dyn = DynamicByteBuffer(8)
		dyn.writeInt16(42)
		dyn.writeInt16(1985)
		dyn.writeInt16(99)
		dyn.writeInt16(2016)
		dyn.writeString("Lucas")

		val dyn2 = DynamicByteBuffer(4)
		dyn2.deepCopy(dyn)
		assertTrue("Original buffer should be valid after deep copy", dyn.isValid())
		assertEquals("Partial position is invalid", 13, dyn2.position)

		dyn2.flip()
		assertEquals(42, dyn2.readUInt16())
		assertEquals(1985, dyn2.readUInt16())
		assertEquals(99, dyn2.readUInt16())
		assertEquals(2016, dyn2.readUInt16())
		assertEquals("Lucas", dyn2.readString(5))
	}

	@Test
	fun testDeepCopyDifferentInitialSize2() {
		val dyn = DynamicByteBuffer(4)
		dyn.writeInt16(42)
		dyn.writeInt16(1985)
		dyn.writeInt16(99)
		dyn.writeInt16(2016)
		dyn.writeString("Lucas")

		val dyn2 = DynamicByteBuffer(8)
		dyn2.deepCopy(dyn)
		assertTrue("Original buffer should be valid after deep copy", dyn.isValid())
		assertEquals("Partial position is invalid", 13, dyn2.position)

		dyn2.flip()
		assertEquals(42, dyn2.readUInt16())
		assertEquals(1985, dyn2.readUInt16())
		assertEquals(99, dyn2.readUInt16())
		assertEquals(2016, dyn2.readUInt16())
		assertEquals("Lucas", dyn2.readString(5))
	}

	@Test
	fun testDeepCopySameInitialSizeWithExistingData() {
		val dyn = DynamicByteBuffer(4)
		dyn.writeInt16(42)
		dyn.writeInt16(1985)
		dyn.writeInt16(99)
		dyn.writeInt16(2016)
		dyn.writeString("Lucas")

		val dyn2 = DynamicByteBuffer(4)
		dyn2.writeInt8(12)
		dyn2.writeInt8(34)
		dyn2.deepCopy(dyn)
		assertTrue("Original buffer should be valid after deep copy", dyn.isValid())
		assertEquals("Partial position is invalid", 15, dyn2.position)

		dyn2.flip()
		assertEquals(12, dyn2.readUInt8())
		assertEquals(34, dyn2.readUInt8())
		assertEquals(42, dyn2.readUInt16())
		assertEquals(1985, dyn2.readUInt16())
		assertEquals(99, dyn2.readUInt16())
		assertEquals(2016, dyn2.readUInt16())
		assertEquals("Lucas", dyn2.readString(5))
	}

	@Test
	fun testDeepCopyDifferentInitialSizeWithExistingData() {
		val dyn = DynamicByteBuffer(8)
		dyn.writeInt16(42)
		dyn.writeInt16(1985)
		dyn.writeInt16(99)
		dyn.writeInt16(2016)
		dyn.writeString("Lucas")

		val dyn2 = DynamicByteBuffer(4)
		dyn2.writeInt8(12)
		dyn2.writeInt8(34)
		dyn2.deepCopy(dyn)
		assertTrue("Original buffer should be valid after deep copy", dyn.isValid())
		assertEquals("Partial position is invalid", 15, dyn2.position)

		dyn2.flip()
		assertEquals(12, dyn2.readUInt8())
		assertEquals(34, dyn2.readUInt8())
		assertEquals(42, dyn2.readUInt16())
		assertEquals(1985, dyn2.readUInt16())
		assertEquals(99, dyn2.readUInt16())
		assertEquals(2016, dyn2.readUInt16())
		assertEquals("Lucas", dyn2.readString(5))
	}

	@Test
	fun testDeepCopyDifferentInitialSizeWithExistingData2() {
		val dyn = DynamicByteBuffer(4)
		dyn.writeInt16(42)
		dyn.writeInt16(1985)
		dyn.writeInt16(99)
		dyn.writeInt16(2016)
		dyn.writeString("Lucas")

		val dyn2 = DynamicByteBuffer(8)
		dyn2.writeInt8(12)
		dyn2.writeInt8(34)
		dyn2.deepCopy(dyn)
		assertTrue("Original buffer should be valid after deep copy", dyn.isValid())
		assertEquals("Partial position is invalid", 15, dyn2.position)

		dyn2.flip()
		assertEquals(12, dyn2.readUInt8())
		assertEquals(34, dyn2.readUInt8())
		assertEquals(42, dyn2.readUInt16())
		assertEquals(1985, dyn2.readUInt16())
		assertEquals(99, dyn2.readUInt16())
		assertEquals(2016, dyn2.readUInt16())
		assertEquals("Lucas", dyn2.readString(5))
	}

	@Test
	fun testCopySameInitialSize() {
		val dyn = DynamicByteBuffer(4)
		dyn.writeInt16(42)
		dyn.writeInt16(1985)
		dyn.writeInt16(99)
		dyn.writeInt16(2016)
		dyn.writeString("Lucas")

		val dyn2 = DynamicByteBuffer(4)
		dyn2.shallowCopy(dyn)
		assertFalse("Original buffer should be invalid after copy", dyn.isValid())
		assertEquals("Partial position is invalid", 13, dyn2.position)

		dyn2.flip()
		assertEquals(42, dyn2.readUInt16())
		assertEquals(1985, dyn2.readUInt16())
		assertEquals(99, dyn2.readUInt16())
		assertEquals(2016, dyn2.readUInt16())
		assertEquals("Lucas", dyn2.readString(5))
	}

	@Test
	fun testCopyDifferentInitialSize() {
		val dyn = DynamicByteBuffer(8)
		dyn.writeInt16(42)
		dyn.writeInt16(1985)
		dyn.writeInt16(99)
		dyn.writeInt16(2016)
		dyn.writeString("Lucas")

		val dyn2 = DynamicByteBuffer(4)
		dyn2.shallowCopy(dyn)
		assertFalse("Original buffer should be invalid after copy", dyn.isValid())
		assertEquals("Partial position is invalid", 13, dyn2.position)

		dyn2.flip()
		assertEquals(42, dyn2.readUInt16())
		assertEquals(1985, dyn2.readUInt16())
		assertEquals(99, dyn2.readUInt16())
		assertEquals(2016, dyn2.readUInt16())
		assertEquals("Lucas", dyn2.readString(5))
	}

	@Test
	fun testCopyDifferentInitialSize2() {
		val dyn = DynamicByteBuffer(4)
		dyn.writeInt16(42)
		dyn.writeInt16(1985)
		dyn.writeInt16(99)
		dyn.writeInt16(2016)
		dyn.writeString("Lucas")

		val dyn2 = DynamicByteBuffer(8)
		dyn2.shallowCopy(dyn)
		assertFalse("Original buffer should be invalid after copy", dyn.isValid())
		assertEquals("Partial position is invalid", 13, dyn2.position)

		dyn2.flip()
		assertEquals(42, dyn2.readUInt16())
		assertEquals(1985, dyn2.readUInt16())
		assertEquals(99, dyn2.readUInt16())
		assertEquals(2016, dyn2.readUInt16())
		assertEquals("Lucas", dyn2.readString(5))
	}

	@Test
	fun testCopySameInitialSizeWithExistingData() {
		val dyn = DynamicByteBuffer(4)
		dyn.writeInt16(42)
		dyn.writeInt16(1985)
		dyn.writeInt16(99)
		dyn.writeInt16(2016)
		dyn.writeString("Lucas")

		val dyn2 = DynamicByteBuffer(4)
		dyn2.shallowCopy(dyn)
		assertFalse("Original buffer should be invalid after copy", dyn.isValid())
		assertEquals("Partial position is invalid", 13, dyn2.position)

		dyn2.flip()
		assertEquals(42, dyn2.readUInt16())
		assertEquals(1985, dyn2.readUInt16())
		assertEquals(99, dyn2.readUInt16())
		assertEquals(2016, dyn2.readUInt16())
		assertEquals("Lucas", dyn2.readString(5))
	}

	@Test
	fun testCopyDifferentInitialSizeWithExistingData() {
		val dyn = DynamicByteBuffer(8)
		dyn.writeInt16(42)
		dyn.writeInt16(1985)
		dyn.writeInt16(99)
		dyn.writeInt16(2016)
		dyn.writeString("Lucas")

		val dyn2 = DynamicByteBuffer(4)
		dyn2.shallowCopy(dyn)
		assertFalse("Original buffer should be invalid after copy", dyn.isValid())
		assertEquals("Partial position is invalid", 13, dyn2.position)

		dyn2.flip()
		assertEquals(42, dyn2.readUInt16())
		assertEquals(1985, dyn2.readUInt16())
		assertEquals(99, dyn2.readUInt16())
		assertEquals(2016, dyn2.readUInt16())
		assertEquals("Lucas", dyn2.readString(5))
	}

	@Test
	fun testCopyDifferentInitialSizeWithExistingData2() {
		val dyn = DynamicByteBuffer(4)
		dyn.writeInt16(42)
		dyn.writeInt16(1985)
		dyn.writeInt16(99)
		dyn.writeInt16(2016)
		dyn.writeString("Lucas")

		val dyn2 = DynamicByteBuffer(8)
		dyn2.shallowCopy(dyn)
		assertFalse("Original buffer should be invalid after copy", dyn.isValid())
		assertEquals("Partial position is invalid", 13, dyn2.position)

		dyn2.flip()
		assertEquals(42, dyn2.readUInt16())
		assertEquals(1985, dyn2.readUInt16())
		assertEquals(99, dyn2.readUInt16())
		assertEquals(2016, dyn2.readUInt16())
		assertEquals("Lucas", dyn2.readString(5))
	}
}
