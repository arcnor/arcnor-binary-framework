package com.arcnor.binary.common

import com.arcnor.binary.ABFEndianness
import com.arcnor.binary.ABFNumberType.UINT8

@ABFObject(endianness = ABFEndianness.LITTLE, fields = arrayOf("myString", "myNum"))
data class TestString(
		@ABFString(hasLength = true)
		var myString: String,
		@ABFNumber(UINT8)
		var myNum: Int
) {
	private constructor() : this("", 0) {}
}

@ABFObject(endianness = ABFEndianness.LITTLE, fields = arrayOf("myString", "myNum"))
data class TestStringAlignment(
		@ABFString(hasLength = true, alignment = 4)
		var myString: String,
		@ABFNumber(UINT8)
		var myNum: Int
) {
	private constructor() : this("", 0) {}
}