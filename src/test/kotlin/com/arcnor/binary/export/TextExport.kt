package com.arcnor.binary.export

import com.arcnor.binary.common.TestString
import com.arcnor.binary.common.TestStringAlignment
import org.junit.Assert
import org.junit.Test
import java.io.ByteArrayOutputStream

class TextExport {
	@Test
	fun testString() {
		val obj1 = TestString("abcde", 16)
		doTest(obj1, "\u0005abcde\u0010".toByteArray())
	}

	@Test
	fun testStringAlignment() {
		val obj1 = TestStringAlignment("abcd", 16)
		doTest(obj1, "\u0004abcd\u0010".toByteArray())
		val obj2 = TestStringAlignment("abcde", 16)
		doTest(obj2, "\u0005abcde\u0000\u0000\u0000\u0010".toByteArray())
	}

	private fun doTest(obj1: Any, expected: ByteArray) {
		val stream = ByteArrayOutputStream()
		ABFExport(obj1).export(stream)
		val result = stream.toByteArray()
		Assert.assertArrayEquals(expected, result)
	}
}
